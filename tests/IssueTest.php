<?php
  /**
   * Created by IntelliJ IDEA.
   * User: phillipisaacs
   * Date: 2019-03-18
   * Time: 22:28
   */
  
  use PHPUnit\Framework\TestCase;
  
  class IssueTest extends TestCase {
  
    static $db_mocks = ['id', 'series', 'number', 'sort', 'printrun', 'quantity', 'cover_date', 'location', 'type', 'status', 'bkcondition', 'cover_price', 'purchase_price', 'purchase_date', 'guide_value', 'guide', 'issue_value', 'comments', '`thumbnail-sm`'];
    
    public function test_Can_Set_A_Series_Id()
    {
      $iss = new ComicDB_Issue();
      $iss->series = 23;
      $this->assertSame(23,$iss->series);
      return $iss;
    }
    
    /**
     * @depends test_Can_Set_A_Series_Id
     * @param $iss - Instance of Issue Class
     */
    public function test_Can_Update_Series_Id($iss)
    {
      $iss->series = 33;
      $this->assertSame(33,$iss->series);
    }
    
    public function test_Attributes_Is_An_Array(){
      $iss = new ComicDB_Issue();
      $issueAttributes = $iss->attributes();
      $this->assertIsArray($issueAttributes);
    }
    
    public function test_Attribute_Has_18_Props(){
      $iss = new ComicDB_Issue();
      $issueAttributes = $iss->attributes();
      $this->assertCount(18, $issueAttributes);
    }
    
    public function test_Attributes_Contain_Series(){
      $iss = new ComicDB_Issue();
      $issueAttributes = $iss->attributes();
      $this->assertArrayHasKey('series',$issueAttributes);
    }
    
    public function test_Attribute_Does_Not_Contain_ID(){
      $iss = new ComicDB_Issue();
      $issueAttributes = $iss->attributes();
      $this->assertArrayNotHasKey('id',$issueAttributes);
    }
    
  }
