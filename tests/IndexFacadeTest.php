<?php
  /**
   * Created by IntelliJ IDEA.
   * User: phillipisaacs
   * Date: 2019-03-20
   * Time: 12:46
   */
  
  namespace tests;
  
  use PHPUnit\Framework\TestCase;
  
  require_once ("app/lib/config.inc");
  require_once ("app/lib/ComicDB/DB.php");
  // require_once ('app/lib/ComicDB/IndexFacade.php');


  
  class IndexFacadeTest extends TestCase
  {
    public $titles;
  
    public function testPushAndPop()
    {
      $stack = [];
      $this->assertSame(0, count($stack));
    
      array_push($stack, 'foo');
      $this->assertSame('foo', $stack[count($stack)-1]);
      $this->assertSame(1, count($stack));
    
      $this->assertSame('foo', array_pop($stack));
      $this->assertSame(0, count($stack));
    }
    
    function testIndexFacadeCanPullAllTitles(){
      $this->markTestSkipped();
      
      $this->titles = new \app\lib\ComicDB\IndexFacade();
    
      
      $this->assertIsArray($this->titles->allTitles);
      
    }
    
  }
