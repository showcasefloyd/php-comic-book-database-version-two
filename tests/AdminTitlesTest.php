<?php
  /**
   * Created by IntelliJ IDEA.
   * User: phillipisaacs
   * Date: 2019-03-20
   * Time: 12:46
   */
  
  namespace tests;
  
  use PHPUnit\Framework\TestCase;
  
  require_once ("app/lib/config.inc");
  require_once ("app/lib/ComicDB/DB.php");
  require_once ("app/lib/ComicDB/Titles.php");
  require_once ("app/lib/ComicDB/Serieses.php");
  require_once ("app/lib/ComicDB/Series.php");
  require_once ("app/lib/ComicDB/Issues.php");

  
  class IndexTitlesTest extends TestCase
  {
    public $titles;
    
    function testAdminCanPull_All_Titles(){
      $titles_list = new \ComicDB_Titles();
      $titles = $titles_list->getTitles();
      
      $this->assertIsArray($titles);
    }
   
    function testAdminCanPull_Every_Series_Without_TitleId(){
      $series_list = new \ComicDB_Serieses();
      $series = $series_list->getAll();
      
      $this->assertIsArray($series);
    }
    
    function testAdminCanPull_All_Series_For_Title_With_TitleId(){
      $title_id = 1;
      $aSeries = new \ComicDB_Serieses($title_id);
      $series = $aSeries->getAll();
      $this->assertIsArray($series);
    }
    
    function testAdminCanPull_All_Issues_For_Title_With_SeriesId(){
      
      $series_id = 1;
      $seriesIssues = new \ComicDB_Issues($series_id);
      $issues = $seriesIssues->getIssues();

      $this->assertIsArray($issues);
      
    }
    
  }
