# Artichoke - an application to catalog and organize your comic collection 


## Updates APril 2021

To spin up your environment you just need to make sure you have to Docker installed and then run `docker-compose up` which will spin up your php-apache container (running at http://localhost:8080) and your MySQL backend. You will also have access to phpMyAdmin at http://localhost:8081

`docker-composer down` will take the containers down as yoy might expect.

### Using Gulp

This also uses Gulp.js for transpiring the SCSS and browser re-loading for now. To get started just type `gulp` which will start watching for php or scss changes and reload the browser.


## Version 2.1.1

### Changes so far

- Composer is still required to install PHP_Unit

- PEAR and the PEAR_DB modules have been completely removed and are no longer required

- Converted main page to install PHP page and removed all old and ugly iFrames

#### Composer / Vendor and PHP


Currently the best way to run this is to install Composer either via Brew (OS X) or apt-get (Linux). And then run the `composer install` which will uses the `composer.json` file to install project dependencies and a `composer.lock` file. Just like NPM 

### To Dos

- __To Do__ Remove Bower and convert all front end requirements to use Node_Modules install. Will require rollup or Webpack

### Features to add 

- Support for multiple covers


### Requirements

- A user can search for comics in his collection by publisher, title, series, tags and or date
- A user can view his collection a grid or a list by publisher, title, series, tags and or date
- A user can view the details for of individual issue in his collection
- A user can export out this collection into other formats
- A user can print his collection
- A user can create a wish list of books he wants to add to the collection
- A user can tag books with keywords they want to sort or organize books by 
- A user share the details and achievements of his collections
- Each book in the collection can also include a front cover and back cover 
- The collection will assign achievements to a users collection
- A user can run reports on his collection
- The collection will provide a dashboard od update information and stats of about the collection
- Can track digital comics as well as physical copies


### Metadata To Add

- Grades - This can be static and not in a database for now since it will never change
- CGC - Need to add this field to issues
- Covers - An issues can have multiple covers and printings
- Printing - This will be a new class which contains which cover (all books have at least one) and what printing it is such as 1st, 2nd, etc
- Issues can have multiple printings associated with it
- Guides - Probably don't need this. It relates to value. You guess versus market data but is arbitrary. Value is just based on sales data.
- Add meta data for digital and physical copies

####

xid=1 (title)
sid=10 (series)
iid=17 (issue) 

####

All lookups will no reside in a meta_values table, and meta_categories table. Let classes, less tables, less code.

Example SQL

    SELECT V.value, V.category, C.id, C.name 
    FROM `meta_values` as V
    LEFT OUTER JOIN `meta_categories` as C 
    ON V.category =  C.id
    WHERE C.id = 4