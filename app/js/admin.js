let prevKey = 1;
$("#new_cover").on('click',function(){
  
  const lastKey = $("#issue_cover_key");
  let key = $(lastKey).data('key');
  if(prevKey == 1){
    prevKey = key + prevKey;
  }
  console.log(prevKey);

  const template = `
    <div class="form-group row">
      <label class="col-sm-3 control-label" for="coverDesc">
        Cover Description
      </label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-small" name="covers[${prevKey}][coverDesc]"
            value=""  placeholder="Description">
      </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-3 control-label" for="coverArtist"> Artist </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" placeholder="Artist" name="covers[${prevKey}][coverArtist]" value="">  
      </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-3 control-label">Cover Image</label>
      <div class="col-sm-8">
        Not Implemented -- Upload an image
      </div>
    </div>

    <hr>`;

  $("#new-covers").append(template);
  prevKey++;
});

$(".remove").on('click', function(){
  let key = $(this).val();
  $("#new-covers").append(`<input type="hidden" name="remove_covers[]" value="${key}">`);
  $('#submit-form').trigger('click');
});

