<?
	require_once('./lib/global.inc');
	
	$titlesList = new ComicDB_Titles();
	$titles = $titlesList->getTitles();
	
	if (isset($_GET['xid'])) {
		$seriesList = new ComicDB_Serieses($_GET['xid']);
		$series = $seriesList->getAll();
	}

	$page_title = ComicDB_pageTitle("Library");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?= $page_title; ?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="css/main.css" media="screen">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
			<? require_once('includes/navbar.php'); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-3" >

      <h5>Titles</h5>
      <div id="left-menu">
        <table class="left-menu-table table table-striped table-bordered">
          <tbody>
          <?php if (count($titles) == 0) { ?>
            <tr><th colspan="2">
            <?php	} else {
              foreach ($titles as $t) {

                $title_name = htmlspecialchars($t->name);
                $title_id = $t->id();

                if (isset($xid) && $xid == $title_id) { ?>

                <tr>
                    <td id='<?= $xid ?>' class="title-icon-open">
                        <i class="fas fa-folder-open"> </i>
                    </td>
                    <td class="title-name-open"><?= $title_name ?></td>
                  </tr>

                <?php } else { ?>

                  <tr>
                    <td class="title-icon-closed"><a href="./index.php?xid=<?= $title_id ?>">
                      <i class="fas fa-folder"></i></a>
                    </td>
                      <td class="title-name-closed"><?= $title_name ?></td>
                  </tr>

                <?php }

                if (isset($xid) && $xid == $title_id) {

                  if (empty($series)) {  ?>

                    <tr><td>&nbsp;</td><td class="title-series"><em>No series for title!<em></td> </tr>

                  <?php } else {

                    foreach ($series as $s) {

                      $series_name = htmlspecialchars($s->name);
                      $series_id = $s->id;
                      $series_vol = $s->volume;

                      if (!empty($series_vol)) {
                        $print_series_vol = ' (' . $series_vol . ')';
                      }
                      $issueLink = "<a href='./index.php?sid=$series_id&xid=$xid'>$series_name $print_series_vol</a>";
                      ?>

                      <tr>
                          <td>&nbsp;</td>
                          <td class="title-series">
                              <?= $issueLink; ?>
                          </td>
                      </tr>

                    <?php
                    }
                  }
                }
              }
          } ?>
          </tbody>
        </table>
      </div>
    </div>

    <div class="col-sm-9" id="main-top">
			
			<?php
      
      if (isset($sid)) {
        $series = $seriesList->getOne($sid); 
        
        $volume = $series->volume;
        $lastIssue = $series->finalIssue;
        $comments = $series->comments;
        $complete = $series->complete;
        $category = $series->category;

        $grid = new Catalog_Grid($series);
        $gridData = $grid->displayGrid();
        $percentComplete = $grid->calcPercentageToComplete();
      

			} else {
				
				$dbStats = new Statistics_Gadgets();

				$collectionStats = $dbStats::select();
				$lastUpdate = $dbStats::latestUpdatedIssue();
				
				include_once('includes/homepage/home_blank.php');
			}
				
      if (isset($gridData)) {
        include_once('includes/homepage/home_series.php');
      }
				
      if (isset($iid)) {

        $issue = new ComicDB_Issue($iid);
        
        $rv = $issue->restore();

        $number = htmlspecialchars($issue->number);
        $printRun = htmlspecialchars($issue->printrun);
        $quantity = $issue->quantity;
        $location = htmlspecialchars($issue->location);
        $type = htmlspecialchars($issue->type);
        $condition = htmlspecialchars($issue->bkcondition);
	      $coverDate = htmlspecialchars($issue->coverDate);
	      $purchaseDate = $issue->purchaseDate;
        $coverPrice = "$". $issue->coverPrice;
        $purchasePrice = "$".  $issue->purchasePrice;
        $priceGuideValue = "$".  $issue->guideValue;
        $issueValue = "$".  $issue->issueValue;
        $priceGuide = htmlspecialchars($issue->guide);
        $comments = htmlspecialchars($issue->comments);
        
        if ($issue->status == 0) {
          $status = "Collected";
        } else if ($issue->status == 1) {
          $status = "For Sale";
        } else if ($issue->status == 2) {
          $status = "Wish List";
        } else {
          $status = "Unknown";
        }
        
        /**
         * Month and Year. Do we want to include Day?
         */
        if ($issue->purchaseDate) {
          $purchaseDate = date('F j, Y', strtotime($issue->purchaseDate));
        }
        if ($issue->coverDate) {
          $coverDate = date('F, Y', strtotime($issue->coverDate));
        }
        
        include_once ('includes/homepage/home_issue.php');
      } ?>
      
    </div>
  </div>

  <div class="row">
    <div class="col-12">
			
			<? require_once('includes/footer.php'); ?>

    </div>
  </div>

</div>
<script>
  $().ready(function(){
      let el = $('#<?= $xid ?>');
      let div = $('#left-menu');
      console.log(el.position());
      let cords = el.position();
      $("div").scrollTop(cords.top);
  });
</script>
</body>
</html>
