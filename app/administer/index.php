<?
  require_once('../lib/global.inc');
  
  // PHP Page Declarations
  $series = [];
  $issues = [];
  $titles = [];

  $currAction = $_GET['action'] ?? NULL;
  $currTitleId = $_GET['title_id'] ?? NULL;
  $currSeriesId = $_GET['series_id'] ?? NULL;
  $currIssueId = $_GET['issue_id'] ?? NULL;

  if (isset($_POST['admin-action'])) {
    $currAction = $_POST['admin-action'] ?? NULL;
    $currTitleId = $_POST['title_id'] ?? NULL;
    $currSeriesId = $_POST['series_id'] ?? NULL;
	  $currIssueId = $_POST['issue_id'] ?? NULL;

    require('actions.php');
  }

  $titlesList = new ComicDB_Titles();
  $titles = $titlesList->getTitles();
  $title_menu = ComicDB_Util::makeDropDown($titles, $currTitleId);

  if (!empty($currTitleId)) {
    $seriesList = new ComicDB_Serieses($currTitleId);
	  $titles = $titlesList->getTitles($currTitleId);
    $series = $seriesList->getAll();
  }

  if (!empty($currSeriesId)) {
    $issuesList = new ComicDB_Issues(($currSeriesId));
    $issues = $issuesList->getIssues();
  }

  $page_title = ComicDB_pageTitle("New Admin Panel");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><? echo $page_title; ?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="../css/main.css" media="screen">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
  
  <div class="row">
    <div class="col-12">
      <? require_once('../includes/navbar.php'); ?>
    </div>
  </div>
  
  <div class="row">
    <div class="col-4">
      <div class="admin-sidebar">

    <?php

      if(count($titles) > 0){
        print "<div><a href='index.php?action=title_new' class='btn btn-primary btn-sm my-2' role='button'>New Title</a></div>";
        print "<ul class='list-group'>";
        foreach ($titles as $title) {
          $current = ($currTitleId == $title->id) ? "list-group-item-warning" : "";
          print "<li class='list-group-item $current'>  <a href='index.php?action=title_edit&title_id=" . $title->id . "'> " . $title->name . "</a></li>";
        }
        print "</ul>";
      }

      if(!empty($currTitleId)){
        print "<div><a href='index.php?action=series_new&title_id=".$currTitleId."' class='btn btn-primary btn-sm my-2' role='button'>New Series</a></div>";
	      print "<ul class='list-group'>";
        if(count($series) > 0){
          foreach ($series as $serie){
            $print_vol = (!empty($serie->volume)) ? " (". $serie->volume . ")" : "";
	          $current = ($currSeriesId == $serie->id) ? "list-group-item-warning" : "";
	          print "<li class='list-group-item $current'> <a href='index.php?action=series_edit&title_id=".$currTitleId."&series_id=".$serie->id."'>".$serie->name ." ". $print_vol ."</a> </li>";
          }
        }
	      print "</ul>";
      }

      if(!empty($currSeriesId)){
	      print "<div><a href='index.php?action=issue_new&title_id=".$currTitleId."&series_id=".$currSeriesId."' class='btn btn-primary btn-sm my-2' role='button'>New Issue</a></div>";

        if(count($issues) > 0){
	        print "<div id='comicgrid-sidebar'>";
            foreach ($issues as $issue){
              $bg = ($currIssueId == $issue->id) ? "own" : "";
              print "<div class='issue-box ". $bg ." '><a href='index.php?action=issue_edit&title_id=".$currTitleId."&series_id=".$currSeriesId."&issue_id=".$issue->id."'>". $issue->number ."</a> </div>";
            }
	        print "</div> ";
        }

      }
    ?>
      </div>
    </div>

    <div class="col-8">
      <?
      include('../includes/alerts.php');

      switch ($currAction){
        case 'title_new':
          include("title_new_form.inc");
        break;
  
        case 'title_edit':
          include("title_form.inc");
          break;
  
        case 'series_new':
          include("series_new_form.inc");
          break;
  
        case 'series_edit':
          include("series_form.inc");
          break;
  
        case 'issue_new':
          include("issue_new_form.inc");
          break;
  
        case 'issue_edit' || 'cover_remove':
          include("issue_form.inc");
          break;
          
        default:
          echo '<div class="card mb-3"><div class="card-body">Choose options from drop down menus on left</div></div>';

        include './menu.php';
      }
      ?>
    </div>
  </div>
  
  <div class="row">
    <div class="col-12">
      <? require_once('../includes/footer.php');  ?>

    </div>
  </div>
</div>
<script src="../js/admin.js"></script>
</body>
</html>
