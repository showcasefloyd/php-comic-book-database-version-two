<?php
require_once('../lib/global.inc');

$book = new ComicDB_Book();
$book->debug = false;

$action = $_GET['action'] ?? NULL;
$ValId = $_GET['valid'] ?? NULL;
$CatId = $_GET['catid'] ?? 12;
$page = $_GET['page'] ?? 0;
$limit = $_GET['limit'] ?? 25;

$category = $book->getMetaCategory($CatId);

if(count($_GET) > 0){	
	if($action == 'del')	{
	  $book->deleteListValue($ValId);
		$success_msg = $category['name'] ." ". $id ." removed ";
	}
};

if(count($_POST) > 0){	
	$action = $_POST['action'] ?? NULL;

	if(($action == 'update'))	{	
    $ValId = $_POST['valid'] ?? NULL;
		$updated_category = $_POST[$category['name']] ?? NULL;
    
    $book->updateListValue($updated_category,$ValId);
		$success_msg = $category['name'] ." ". $updated_category ." changed ";
	}

	if(($action == 'add'))	{		
		$new_category = $_POST[$category['name']] ?? NULL;
	  if(empty($new_category)){
			$error_msg = "Please include one category name";
		} else {
			$book->addListValue($new_category,$CatId);
			$success_msg = "New ". $category['name'] ." ". $new_category ." added ";
		}
	}	
};

// @TODO - Need to refactor for new data model
//$records = $data->getAll($limit,$page);
//$data->updateTotal();

$data = $book->getList($CatId);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><? echo $page_title; ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale  =1.0">

	<link rel="stylesheet" href="../css/main.css" media="screen">
	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">

	<div class="row">
		<div class="col-12">
			<? require_once('../includes/navbar.php'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<? require_once('../includes/alerts.php'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div id="crud">
			
			<h3><?= $category['name']; ?></h3>

			<hr>

			<div class="my-3 py-2">
				<?php if($action == 'new' ){ ?>
					<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?catid=<?= $CatId ?>">
						<div class="row">
							<div class="col-4">
								<input name="<?= $category['name'] ?>" class="form-control my-1" value="">
							</div>
							<div class="col-8">
								<button type="submit" class="btn btn-sm btn-primary mt-2">Add</button>
								<input type="hidden" name="action" value="add">
								<input type="hidden" name="catid" value="<?= $CatId ?>">
							</div>
						</div>
					</form>
				<?php	} else { ?>
					<a href="<?= $_SERVER['PHP_SELF'] ?>?action=new&catid=<?= $CatId ?>" class="btn btn-sm btn-primary mx-0">New <?= $category['name'] ?></a>
				<? } ?>
			</div>

			<table class="table table-sm">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">ID</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>

					<?php
					$i = ($limit * $page) === 0  ? 1 : ($limit * $page);
					foreach ($data as $cat){ 
					?>
						<tr>
								<th scope="row"><?= $i ?></th>
								<td><?= $cat['name'] ?></td>	
								<td><?= $cat['ValKey'] ?></td>	
								<td>
									<a href="<?= $_SERVER['PHP_SELF'] ?>?action=edit&valid=<?= $cat['ValKey'] ?>&page=<?= $page ?>&catid=<?= $CatId ?>" class="btn btn-sm btn-primary">Edit</a>
									<a href="<?= $_SERVER['PHP_SELF'] ?>?action=del&valid=<?= $cat['ValKey'] ?>&page=<?= $page ?>&catid=<?= $CatId ?>" class="btn btn-sm btn-danger">Delete</a>
								</td>
						</tr>
						
						<?php if($action == 'edit' && $valid == $cat['ValKey'] ){ ?>
							<tr>
								<td colspan="4" class="border border-primary">
									<div class="my-3 p-1">
										<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $page ?>&catid=<?= $CatId ?>">
											<input name="<?= $category['name'] ?>" value="<?= $cat['name'] ?>">
											<button type="submit" class="btn btn-sm btn-primary">Update</button>
											<input type="hidden" name="valid" value="<?= $cat['ValKey'] ?>">
											<input type="hidden" name="action" value="update">
										</form>
									</div>
								</td>
							</tr>
						<?php	} ?>

					<?
						$i++; 
						} 
					?>

					</tbody>
				</table>			

      
      </div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
				
			<? new Catalog_Pagination($data->total, $limit, $page) ?>
			<? require_once('../includes/footer.php'); ?>

		</div>
	</div>
</div>

</body>
</html>