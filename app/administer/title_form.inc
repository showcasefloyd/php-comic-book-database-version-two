<?php

$title = new ComicDB_Title($title_id);
$rv = $title->restore();

if ($rv['error']) {
	$error_msg = "Can't restore title: " . $rv['errmsg'];
}

?>

<form class="" method="post" action="index.php">

  <div class="card">
    <div class="card-header">
      <span class="card-title">Update Title</span>
    </div>

    <div class="card-body">
      <div class="form-group row">

        <label class="col-2 control-label" for="inputTitle">
         Name <span class="mandatory-field-marker">*</span>
        </label>

        <div class="col-10">
          <input
            type="text"
            class="form-control"
            name="title_name"
            for="inputTitle"
            value="<?= $title->name; ?>"
            maxlength="80"
          >
          <small class="text-muted">A title can be a group of series or a single stand alone book</small>
        </div>
      </div>
    </div>

    <div class="card-footer">
      <p><b>Warning:</b> Deleting a title will remove all issues and series associated with the title. </p>
      <input type="hidden" name="title_id" value="<?= $title->id(); ?>">

      <button
        type="submit"
        class="btn btn-primary btn-sm"
        name="admin-action"
        value="title_edit"
      >
        Update Title
      </button>

      <button
        type="submit"
        class="btn btn-sm btn-danger"
        name="admin-action"
        value="title_remove"
      >
        Remove Title
      </button>

    </div>

  </div>
</form>
