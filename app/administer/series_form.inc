<?php

$title = new ComicDB_Title($title_id);
$rv = $title->restore();

if ($rv['error']) {
	$error_msg = "Can't restore title: " . $rv['errmsg'];
}

$book = new ComicDB_Book();
$book->debug = false;
$categories = $book->getList(12);
$publishers = $book->getList(26);

$series = new ComicDB_Series($series_id);
$rvs = $series->restore();

if ($rvs['error']) {
	$error_msg = "Can't restore series: " . $rvs['errmsg'];
}

if (!isset($save_series) || (isset($save_series) && isset($success_msg))) {

	$series_name = $series->name;
	$default_price = $series->defaultPrice;
	$first_issue = $series->firstIssue;
	$last_issue = $series->finalIssue;
	$subscribed = $series->subscribed;
	$complete = $series->complete;
	$comments = htmlspecialchars($series->comments);
	$volume = htmlspecialchars($series->volume);
}
?>

<form class="form-horizontal" method="post" action="index.php">
  <div class="card">
    <div class="card-header">
      <span class="card-title">Update Series</span>
      <input type="hidden" name="title_id" value="<?= $title->id() ?>">
      <input type="hidden" name="series_id" value="<?= $series->id ?>">
    </div>

    <div class="card-body">
      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle"> Title </label>
        <div class="col-sm-8">
          <b><?= htmlspecialchars($title->name); ?></b>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputSeries">
          Series <span class="mandatory-field-marker">*</span>
        </label>
        <div class="col-sm-8">
          <input
            type="text"
            class="form-control"
            id="inputSeries"
            name="series_name" value="<?= $series_name ?>"
            maxlength="80">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle">Volume</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" placeholder="Volume or Year" name="volume" value="<?= $volume ?>">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Publisher <span class="mandatory-field-marker">*</span> </label>
        <div class="col-sm-8">
          <select class="form-control" name="publisher">
            <option value=""> --choose--</option>
						<?php
						foreach ($publishers as $pub) {
							//$name = htmlspecialchars($p->name());
							if ($series->publisher == $pub['ValKey']) {
								echo '<option value="'. $pub['ValKey'] .'" selected>'. $pub['name'] .'</option>';
							} else {
								echo '<option value="'. $pub['ValKey'] .'">'. $pub['name'] .'</option>';
							}
						}
						?>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Category</label>
        <div class="col-sm-8">
          <select class="form-control" name="category">
            <option value=""> --choose--</option>
            <?php
            foreach ($categories as $cat) {
              //$name = htmlspecialchars($s->name());
              if ($series->category == $cat['ValKey']) {
                echo '<option selected value="'. $cat['ValKey'] .'">'. $cat['name'] .'</option>';
              } else {
                echo '<option value="'. $cat['ValKey'] .'">'. $cat['name'] .'</option>';
              }
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Default Price <span class="mandatory-field-marker">*</span></label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="default_price" placeholder="$3.99" value="<?= $default_price ?>"
                 maxlength="12">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Issues <span class="mandatory-field-marker">*</span> </label>
        <div class="col-sm-3">
          <input type="text" class="form-control" placeholder="Number of Issues" name="last_issue" value="<?= $last_issue ?>">
        </div>
      </div>



      <div class="form-group row">
        <div class="offset-3 col-10">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="subscribed" <?= $subscribed ? "checked" : "" ?>> Subscribed ?
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="complete" <?= $complete ? "checked" : "" ?>> Complete ?
            </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label class="control-label col-sm-3">Comments</label>
        <div class="col-sm-8">
          <textarea name="comments" class="form-control" rows="6" cols="80"><?= $comments ?></textarea></p>
        </div>
      </div>
    </div>
    <div class="card-footer">

      <p><b>Warning:</b> Deleting a series will remove all issues associated with the series. </p>

      <button type="submit" class="btn btn-primary btn-sm" name="admin-action" value="series_edit">Update Series</button>
      <button type="submit" class="btn btn-danger btn-sm" name="admin-action" value="series_remove">Remove Series</button>

    </div>
  </div>

</form>
