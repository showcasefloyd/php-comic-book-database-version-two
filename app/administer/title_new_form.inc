<form class="" method="post" action="index.php">

  <div class="card">
    <div class="card-header">
      <span class="card-title">Add New Title</span>
    </div>

    <div class="card-body">
      <div class="form-group row">

        <label class="col-2 control-label" for="inputTitle">
          Name <span class="mandatory-field-marker">*</span>
        </label>

        <div class="col-10">
          <input
            type="text"
            class="form-control"
            name="title_name"
            for="inputTitle"
            value=""
            maxlength="80"
          >
          <small class="text-muted">A title can be a group of series or a single stand alone book</small>
        </div>
      </div>
    </div>

    <div class="card-footer">

      <button
        type="submit"
        class="btn btn-primary btn-sm"
        name="admin-action"
        value="title_new"
      >
        Add New Title
      </button>


    </div>

  </div>
</form>