<div class="row">
	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Publishers</h5>
				<p class="card-text">Add and updated Publishers</p>
				<a href="metadata.php?catid=26" class="btn btn-primary">Manage Publishers</a>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Categories</h5>
				<p class="card-text">What new Categories? Yeah, every day. Edit or remove.</p>
				<a href="metadata.php?catid=12" class="btn btn-primary">Manage Categories</a>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Locations</h5>
				<p class="card-text">Add and updated Locations</p>
				<a href="metadata.php?catid=6" class="btn btn-primary">Manage Locations</a>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Printing</h5>
				<p class="card-text">12st, 2nd, 4th?</p>
				<a href="metadata.php?catid=3" class="btn btn-primary">Manage Printings</a>
			</div>
		</div>
	</div>
</div>