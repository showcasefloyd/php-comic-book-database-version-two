<?php

/**
 * @todo: Cover Date and Purchased Date are wrong when added to the database 
 *        Should be month and year
 * @todo: Add missing metadata locations, printings, cover
 */

$title = new ComicDB_Title($title_id);
$title->restore();

$series = new ComicDB_Series($series_id);
$series->restore();

$issue = new ComicDB_Issue($issue_id);
$issue->restore();

$book = new ComicDB_Book();
$grades = $book->getList(5);
$guides = $book->getList(4);
$locations = $book->getList(6);
$printing = $book->getList(3);

// SAVE
if (!isset($save_issue) || (isset($save_issue) && isset($success_msg))) {

	/**
	 * Time stamps are not Dates.
	 * You must convert a Date / Date object into a timestamp and vice versus
	 */
}
?>

<form class="form-horizontal" method="post" action="index.php">
  <div class="card">
    <div class="card-header">

      <span class="card-title">Update Issue</span><br>
      <input type="hidden" name="issue_id" value="<?= $issue->id; ?>">
      <input type="hidden" name="title_id" value="<?= $title->id; ?>">
      <input type="hidden" name="series_id" value="<?= $series->id ?>">
    </div>

    <div class="card-body">

      <div class="form-group row">
        <label class="col-sm-3"> Title </label>
        <div class="col-sm-8">
          <b><?= htmlspecialchars($title->name); ?></b>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label"> Series </label>
        <div class="col-sm-8">
          <b><?= htmlspecialchars($series->name)." (". $series->volume . ")" ?></b>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputIssueNum">
          Issue Number <span class="mandatory-field-marker">*</span>
        </label>
        <div class="col-sm-3">
          <input type="text" class="form-control form-small" name="number" for="inputIssueNum"
                 value="<?= $issue->number; ?>" size="2" maxlength="10">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label"> Print Run</label>
        <div class="col-sm-8">
          <select class="form-control" name="print_run">
            <option value=""> --choose--</option>
            <?php
            foreach ($printing as $print) {
              //$name = htmlspecialchars($p->name());
              if ($issue->printrun == $print['ValKey']) {
                echo '<option value="'. $print['ValKey'] .'" selected>'. $print['name'] .'</option>';
              } else {
                echo '<option  value="'. $print['ValKey'] .'" >'. $print['name'] .'</option>';

              }
            }
            ?>
          </select>

        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputQuantity"> Quantity</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="quantity" value="<?= $issue->quantity; ?>" maxlength="11">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Cover Date</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="cover_date" value="<?= $issue->coverDate; ?>"
                 placeholder="yyyy-mm-dd" maxlength="80">
          <small class="text-muted">Dates are stored as YYYY-MM-DD <br>( Ex: 1998-09-01 ) in database</small>
        </div>
      </div>

      <div class="form-group row">
        <div class="offset-3 col-9">

          <div class="radio-inline">
            <label class="radio-inline">
              <?php if ($issue->status == 0) { ?>
                <input type="radio" name="status" value="0" checked>
              <?php } else { ?>
                <input type="radio" name="status" value="0">
              <?php } ?> Collected:
            </label>

            <label class="radio-inline">
              <?php if ($issue->status == 1) { ?>
                <input type="radio" name="status" value="1" checked>
              <?php } else { ?>
                <input type="radio" name="status" value="1">
              <?php } ?> For Sale:
            </label>

            <label class="radio-inline">
              <?php if ($issue->status == 2) { ?>
                <input type="radio" name="status" value="2" checked>
              <?php } else { ?>
                <input type="radio" name="status" value="2">
              <?php } ?> Wish List:
            </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Condition</label>
        <div class="col-sm-8">
          <select class="form-control" name="bkcondition">
            <option value="">--choose--</option>
            <?php
            for ($row = 0; $row < count($grades); $row++) {
              // $name = htmlspecialchars($c->name);
              if ($issue->bkcondition == $grades[$row]['value']) {
                echo '<option value="'.$grades[$row]['value'] .'" selected>'. $grades[$row]['name'] .'</option>';
              } else {
                echo '<option value="'.$grades[$row]['value'] .'">'. $grades[$row]['name'] .'</option>';
              }
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Cover Price</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="cover_price" value="<?= $issue->coverPrice; ?>" maxlength="12">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Purchase Price</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="purchase_price" value="<?= $issue->purchasePrice; ?>"
                 maxlength="12">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Purchase Date</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="purchase_date" value="<?= $issue->purchaseDate; ?>"
                 placeholder="yyyy-mm-dd" maxlength="12">
          <small>Dates are stored as YYYY-MM-DD <br>( Ex: 1998-09-01 ) in database</small>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Guide Value</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="guide_value" value="<?= $issue->guideValue; ?>" maxlength="12">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Price Guide</label>
        <div class="col-sm-8">
          <select class="form-control" name="guide">
            <option value=""> --choose--</option>
						<?php
						foreach ($guides as $g) {
							//$name = htmlspecialchars($g->name());
							if ($issue->guide  == $g['ValKey']) {
								echo '<option value="'.$g['ValKey'].'" selected>'. $g['name'] .'</option>';
							} else {
								echo '<option value="'.$g['ValKey'].'">'. $g['name'] .'</option>';
							}
						}
						?>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Value of Issue</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="issue_value" value="<?= $issue->issueValue; ?>" maxlength="12">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Comments</label>
        <div class="col-sm-8">
          <textarea name="comments" class="form-control" rows="4" cols="60"><?= $issue->comments ?></textarea>
        </div>
      </div>

      <?php
        include('./issue_cover.inc'); 
      ?>
      
    </div>

    <div class="card-footer">
      <button type="submit" id="submit-form" class="btn btn-primary btn-sm" name="admin-action" value="issue_edit">Update Issue</button>
      <button type="submit" class="btn btn-danger btn-sm" name="admin-action" value="issue_remove">Remove Issue</button>
    </div>

  </div>
</form>


