<?php

// Declare PHP Page variables
$title_name = $title_name ?? null;
$series_name = $series_nam ?? null;
$default_price = $default_price ?? null;
$subscribed = $subscribed ?? null;
$complete = $complete ?? null;
$comments = $comments ?? null;

$title = new ComicDB_Title($title_id);
$rv = $title->restore();

$book = new ComicDB_Book();
$categories = $book->getList(12);
$publishers = $book->getList(26);

// Declare PHP variable
$save_new_series = $save_new_series ?? null;

/**
 * If we are displaying the form for the first time, set default form field values
 */

if (!$save_new_series) {
	$first_issue = 1;
	$last_issue = 0;
}
?>

<form class="form-horizontal" method="post" action="index.php">

  <div class="card">
    <div class="card-header">
      <span class="card-title">New Series</span>
      <input type="hidden" name="title_id" value="<?= $title->id ?>">
    </div>
    <div class="card-body">

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle"> Title </label>
        <div class="col-sm-8">
          <b><?= htmlspecialchars($title->name); ?></b>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle">
          Series <span class="mandatory-field-marker">*</span></label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="series_name" value="<?= $series_name ?>" maxlength="80">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle">Volume</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" placeholder="Volume or Year" name="volume">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle">
          Publisher <span class="mandatory-field-marker">*</span></label>
        <div class="col-sm-8">

          <select class="form-control" name="publisher">
            <option value=""> --choose--</option>
            <?php
						foreach ($publishers as $pub) {
							//$name = htmlspecialchars($p->name());
							if ($series->publisher == $pub['ValKey']) {
								echo '<option value="'. $pub['ValKey'] .'" selected>'. $pub['name'] .'</option>';
							} else {
								echo '<option value="'. $pub['ValKey'] .'">'. $pub['name'] .'</option>';
							}
						}
						?>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle">Category</label>
        <div class="col-sm-8">

          <select class="form-control" name="category">
            <option value=""> --choose--</option>
						<?php
              foreach ($categories as $cat) {
                //$name = htmlspecialchars($s->name())
  
                if ($series->category == $cat['ValKey']) {
                  echo '<option selected value="'. $cat['ValKey'] .'">'. $cat['name'] .'</option>';
                } else {
                  echo '<option value="'. $cat['ValKey'] .'">'. $cat['name'] .'</option>';
                }
              }
						?>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label" for="inputTitle">Default Price  <span class="mandatory-field-marker">*</span></label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="default_price" placeholder="$3.99" value="<?= $default_price ?>"  maxlength="12">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 control-label">Issues <span class="mandatory-field-marker">*</span> </label>
        <div class="col-sm-3">
          <input type="text" class="form-control" placeholder="Number of Issues" name="last_issue">
        </div>
      </div>

      <div class="form-group row">
        <div class="offset-3 col-10">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="subscribed" <?= $subscribed ? "checked" : "" ?>> Subscribed ?
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="complete" <?= $complete ? "checked" : "" ?>> Complete ?
            </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label class="control-label col-sm-3">Comments</label>
        <div class="col-8">
          <textarea name="comments" class="form-control" rows="6" cols="80"><?= $comments ?></textarea></p>
        </div>
      </div>

    </div>
    <div class="card-footer">

      <button type="submit" class="btn btn-primary btn-sm" name="admin-action" value="series_new">Add New Series</button>
    </div>

  </div>
</form>
