<?php

  function addCovers($covers,$id){
    // Save Cover
    foreach($covers as $c){
      $cover = new ComicDB_Cover();
      $cover->debug = false;
      $cover->issueId = $id;
      $cover->artist = $c['coverArtist'];
      $cover->description = $c['coverDesc'];
      $rv = $cover->save();
    }
  }

  function editCovers($covers,$id){
    // Edit Cover
    foreach($covers as $c){
  
      $cover = new ComicDB_Cover($c['id']);
      $cover->debug = false;
      $cover->issueId = $id;
      $cover->artist = $c['coverArtist'];
      $cover->description = $c['coverDesc'];
      $rv = $cover->save();
    }
  }

  function removeCovers($covers,$id){
    // Delete Cover
    foreach($covers as $c){
      $cid = (is_array($c)) ? $c['id'] : $c ;
      $cover = new ComicDB_Cover($cid);
      $cover->debug = false;
      $cover->issueId = $id;
      $rv = $cover->remove();
    }

  }

  // Title Attributes 
	$title_name = $_POST['title_name'] ?? NULL;
	$title_id = (int)$_POST['title_id'] ?? NULL;
  
  // Series Attributes
	$series_id = (int)$_POST['series_id'] ?? NULL;
	$series_name = $_POST['series_name'] ?? NULL;
	$publisher = (int)$_POST['publisher'] ?? NULL;
	$category = (int)$_POST['category'] ?? NULL; // Genre
  $first_issue = $_POST['first_issue'] ?? NULL; // N/A
	$last_issue = $_POST['last_issue'] ?? NULL; // issues
	$default_price = $_POST['default_price'] ?? NULL;
	$subscribed = $_POST['subscribed'] ?? NULL;
	$complete = $_POST['complete'] ?? NULL;
	$volume = $_POST['volume'] ?? NULL;
	$location  = $_POST['location'] ?? NULL; // Missing on form (storage box)
	$type = $_POST['type'] ?? NULL;  // Missing on form (mini, one, shot)
	$comments = $_POST['comments'] ?? NULL;  // Both series and issues can have comments
  
  // Issue Attributes
	$print_run = $_POST['print_run'] ?? NULL;  // should be printing (1st, 2nd, 3rd)
	$issue_id = (int)$_POST['issue_id'] ?? NULL;
	$number = $_POST['number'] ?? NULL;
	$status = $_POST['status'] ?? NULL; // Unsure (holding, sell, out to CGC)
	$quantity = $_POST['quantity'] ?? NULL; // How many copies
	$bkcondition = $_POST['bkcondition'] ?? NULL;
	$purchase_price = $_POST['purchase_price'] ?? NULL;
	$purchase_date = $_POST['purchase_date'] ?? NULL;
	$guide_value = $_POST['guide_value'] ?? NULL;
	$guide = $_POST['guide'] ?? NULL;
	$issue_value = $_POST['issue_value'] ?? NULL;
  
  // Covers
	$cover_price = $_POST['cover_price'] ?? NULL;
	$cover_date = $_POST['cover_date'] ?? NULL;
  $covers = $_POST['covers'] ?? NULL;
  $remove_covers = $_POST['remove_covers'] ?? NULL;

  switch ($currAction) {
    
    case 'title_new':
      if ($title_name) {
        
        $title = new ComicDB_Title();
        $title->name = $title_name;
        
        $rv = $title->save();
        
        if ($rv) {
          $success_msg = "New title '$title->name' Added";
        } else {
          $error_msg = "Can't add new title: $rv->message [$rv->debuginfo]";
        }
        
      } else {
        $error_msg = "All mandatory fields must be filled in.";
      }
      
      break;
    
    case 'title_edit':
      
      if ($title_name) {

        $title = new ComicDB_Title($title_id);
        $title->name = $title_name;
        
        $rv = $title->save();
        
        if ($rv['error']) {
          $error_msg = "Can't update title: ".$rv['errmsg'];
        } else {
          $success_msg = "Updated title:  ".$title_name;
        }
        
      } else {
        $error_msg = "All mandatory fields must be filled in.";
      }
      break;
      
    case 'title_remove':

      $title = new ComicDB_Title($title_id);
      
      $rv = $title->remove();
      
      if ($rv['error']) {
        $error_msg = "Can't remove title: ".$rv['errmsg'];
      } else {
        $success_msg = "Removed title.";
      }
      
      break;
    
    case 'series_new':

    	if (!empty($title_id) && !empty($series_name) && !empty($publisher)) {

        $series = new ComicDB_Series();

			  $series->titleId = $title_id;
			  $series->name = $series_name;
			  $series->publisher = $publisher;
			  $series->category = $category;
			  $series->defaultPrice = $default_price;
			  $series->firstIssue = $first_issue;
			  $series->finalIssue = $last_issue;
			  $series->subscribed = ($subscribed === 'on') ? 1 : 0;
	      $series->complete = ($complete === 'on') ? 1 : 0;
			  $series->comments = $comments;
			  $series->volume = $volume;

        $rv = $series->save();
        
        if ($rv) {
          $success_msg = "Added series '$series_name'.";
          $series_id = $series->id();
        } else {
          $error_msg = "Can't add series: $rv->message [$rv->debuginfo]";
        }
        
      } else {
        $error_msg = "All mandatory fields for series must be filled in.";
      }
      
      break;
    
    case 'series_edit':

	    if (!empty($title_id) && !empty($series_name) && !empty($publisher)) {
        
        $series = new ComicDB_Series($series_id);

	      $series->titleId = $title_id;
	      $series->name = $series_name;
	      $series->publisher = $publisher;
	      $series->category = $category;
	      $series->defaultPrice = $default_price;
	      $series->firstIssue = $first_issue;
	      $series->finalIssue = $last_issue;
	      $series->subscribed = ($subscribed === 'on') ? 1 : 0;
	      $series->complete = ($complete === 'on') ? 1 : 0;
	      $series->comments = $comments;
	      $series->volume = $volume;
        
        $rv = $series->save();

        if ($rv) {
          $success_msg = "Updated series '$series_name'.";
        } else {
          $error_msg = "Can't update series: $rv->message [$rv->debuginfo]";
        }
        
      } else {
		    $error_msg = "All mandatory fields for series must be filled in.";
      }
      
      break;
    
    case 'series_remove':
      
      $series = new ComicDB_Series($series_id);
      
      $rv = $series->remove();
      if ($rv) {
        $success_msg = "Removed series.";
      } else {
        $error_msg = "Can't remove series: $rv->message [$rv->debuginfo]";
      }
      
      break;
    
    case 'issue_new':
	    if (!empty($series_id) && (strval($number) !== "")) {
        
        $issue = new ComicDB_Issue();
        $issue->debug = false;

        $issue->series = $series_id;
        $issue->number = $number;
        $issue->printrun = $print_run;
        $issue->quantity = $quantity;
        $issue->coverDate = $cover_date;
        $issue->location = $location;
        $issue->type = $type;
        $issue->status = $status;
        $issue->bkcondition = $bkcondition;
        $issue->coverPrice = $cover_price;
        $issue->purchasePrice = $purchase_price;
        $issue->purchaseDate = $purchase_date;
        $issue->guideValue = $guide_value;
        $issue->guide = $guide;
        $issue->issueValue = $issue_value;
        $issue->comments = $comments;
        
        $id = $issue->save();

        addCovers($covers,$id);
        
        if ($rv) {
          $success_msg = "Added issue '$number'.";
          $issue_id = $issue->id(); 
          $currAction = "issue_edit";

        } else {
          $error_msg = "Can't add issue: $rv->message [$rv->debuginfo]";
        }
        
      } else {
        $error_msg = "All mandatory fields must be filled in.";
      }
      break;
    
    case 'issue_edit' || 'cover_remove':
	    if (!empty($issue_id) && !empty($series_id) && (strval($number) !== "")) {
        
        $issue = new ComicDB_Issue($issue_id);
        $issue->debug = false;

	      $issue->series = $series_id;
	      $issue->number = $number;
	      $issue->printrun = $print_run;
	      $issue->quantity = $quantity;
	      $issue->coverDate = $cover_date;
	      $issue->location = $location;
	      $issue->type = $type;
	      $issue->status = $status;
	      $issue->bkcondition = $bkcondition;
	      $issue->coverPrice = $cover_price;
	      $issue->purchasePrice = $purchase_price;
	      $issue->purchaseDate = $purchase_date;
	      $issue->guideValue = $guide_value;
	      $issue->guide = $guide;
	      $issue->issueValue = $issue_value;
	      $issue->comments = $comments;
        
        $rv = $issue->save();

        if(!$covers[0]['id']){
          addCovers($covers,$issue_id);
        } else {
          editCovers($covers,$issue_id);
        }

        if($remove_covers != null){
          removeCovers($remove_covers,$issue_id);
        }

        if ($rv) {
          $success_msg = "Updated issue '$number'.";
        } else {
          $error_msg = "Can't update issue: $rv->message [$rv->debuginfo]";
        }
      } else {
        $error_msg = "All mandatory fields must be filled in.";
      }
      break;
    
    case 'issue_remove':
      $issue = new ComicDB_Issue($issue_id);
      $rv = $issue->remove();

      removeCovers($covers,$issue_id);

	    if ($rv) {
		    $success_msg = "Issue removed.";
	    } else {
		    $error_msg = "Can't remove issue: $rv->message [$rv->debuginfo]";
	    }
      break;
  }