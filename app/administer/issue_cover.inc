<?php

$cover = new ComicDB_Covers($issue_id);
$covers = $cover->getAll() ?? [];

?>

<div class="card my-3">
  <div class="card-header">
    <span class="card-title">Covers</span> 
    <? if($currAction == 'issue_edit'){ ?>
      <button type="button" class="btn btn-primary btn-sm float-right" name="admin-action" id="new_cover" value="cover_new">Add New Cover</button>
    <?php } ?>
  </div>

  <div class="card-body">
  
  <?php if($covers != null){ 
    foreach($covers as $key => $cover) { ?>
    
        <div class="form-group row">
          <label class="col-sm-3 control-label" for="coverDesc">
            Cover Description
          </label>
          <div class="col-sm-6">
            <input type="text" class="form-control form-small" name="covers[<?= $key ?>][coverDesc]"
                value="<?= $cover['description'] ?>"  placeholder="Description">
          </div>
        </div>
    
        <div class="form-group row">
          <label class="col-sm-3 control-label" for="coverArtist"> Artist </label>
          <div class="col-sm-8">
            <input type="text" class="form-control" placeholder="Artist" name="covers[<?= $key ?>][coverArtist]" value="<?= $cover['artist'] ?>">  
          </div>
        </div>
    
        <div class="form-group row">
          <label class="col-sm-3 control-label">Cover Image</label>
          <div class="col-sm-8">
            Not Implemented -- Upload an image
          </div>
        </div>

        <div class="form-group row">   
          <div class="col-sm-12">     
            <button type="button" class="btn btn-primary btn-sm float-right remove" name="admin-action" value="<?= $cover['id'] ?>">Remove Cover</button>
            <input type="hidden" data-key="<?= $key ?>" name="covers[<?= $key ?>][id]" value="<?= $cover['id'] ?>">  
          </div> 
        </div>
        <hr>

        <div id="new-covers"></div>

        <?php } ?>

        <input type="hidden" id="issue_cover_key" data-key="<?= $key ?>" name="covers-key" value="<?= $key ?>"> 
      
      <?php } else { ?>

        <div class="form-group row">
          <label class="col-sm-3 control-label" for="coverDesc">
            Cover Description
          </label>
          <div class="col-sm-6">
            <input 
              type="text"
              class="form-control form-small" 
              name="covers[<?= ($key + 1)  ?>][coverDesc]"
              value="" 
              placeholder="Description">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-3 control-label" for="coverArtist"> Artist </label>
          <div class="col-sm-8">
            <input 
              type="text" 
              class="form-control" 
              placeholder="Artist" 
              name="covers[<?= ($key + 1)  ?>][coverArtist]" 
              value="">  
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-3 control-label">Cover Image</label>
          <div class="col-sm-8">
            Not Implemented -- Upload an image
          </div>
        </div>

      <?php } ?>

  </div>
    