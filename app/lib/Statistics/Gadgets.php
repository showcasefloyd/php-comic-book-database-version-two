<?php
  
  /**
   * Created by IntelliJ IDEA.
   * User: phillipisaacs
   * Date: 2019-03-02
   * Time: 21:56
   */
  
  class Statistics_Gadgets{
    
    // Accessors
    public static function select(){
      $query = "select * from stats LIMIT 1";
      $db = ComicDB_DB::db();
      $db->query($query);
      if (!$result = $db->query($query)) {
        return('There was an error running the query [' . $db->error . ']');
      }
      $row = $result->fetch_array();
    
      return $row;
    }

    public static function latestUpdatedIssue(){
      $query = "select i.number, i.last_change, s.name, s.publisher, s.volume
          from issues as i
          inner join series as s
          on i.series = s.id
          order by i.last_change DESC
          LIMIT 1";
      
      $db = ComicDB_DB::db();;
      $db->query($query);
      
      if (!$result = $db->query($query)) {
        die('There was an error running the query [' . $db->error . ']');
      }
      
      $row = $result->fetch_array();
    
      return $row;
    }
    
  }