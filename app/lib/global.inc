<?php

$libdir = dirname(realpath(__FILE__));
$include_path = $libdir .":". ini_get("include_path") .":". "../";
ini_set("include_path", $include_path);

include_once("config.inc");

function printDevBanner(){

	$devBanner  = "<div class='alert alert-warning'>";
	$devBanner .= "<p class='text-center'><i>Development ENV:</i> ". DB_URL ." <i>Development Database:</i> ". DB_NAME . "</p>";
	$devBanner .= "</div><hr>";

	return $devBanner;
}

function printVarBanner($vars, $type = 'Get'){
	if(!empty($vars)){
		$devBanner  = "<div class='debug_footer'>";
		foreach($vars as $name => $value) {
			$devBanner .= "<pre>$type [ <b>key:</b> " . $name . ", <b>value:</b> " . $value . " ]</pre>";
		}
		$devBanner .= "</div>";

		return $devBanner;
	}
}

function debugMsg($string){

	$printMsg   = "<div class='alert alert-info'>";
	$printMsg  .= "<p class='text-danger'>$string</p>";
	$printMsg  .= "</div>";

	echo $printMsg;
}

function ComicDB_pageTitle() {
	$chunks = func_get_args();
	array_unshift($chunks, HTML_TITLE_PREFIX);
	return htmlspecialchars(implode(HTML_TITLE_SEPARATOR, $chunks));
}

// Grabs all the values pass to the app and creates variables for them.
foreach($_GET as $name => $value) {
  $$name = $value;
}

// Define my autoLoad Function and then load it using the spl_autoload_register function
function autoload($classname){
  
  $classname = ltrim($classname,'\\');
  $filename = '';
  
  $filename .= str_replace('_', DIRECTORY_SEPARATOR, $classname). '.php';
  
  include ($filename);
}

spl_autoload_register('autoload');
