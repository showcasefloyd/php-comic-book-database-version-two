<?php

/**
 * Abstract Class ComicDB_Metadata
 * @todo - Rename this to attributes 
 */

abstract class ComicDB_Metadata extends ComicDB_Object {

  public $category;

   // Force Extending class to define this method
  abstract function updateTotal();
  abstract function getAll();
  abstract function getOne();

  public function getMetaCategory($category = null){

    $query   = "SELECT * FROM `meta_categories` ";
    $query  .= "WHERE id = ". $category ." ";
    $query  .= "AND `active` = true";
     
    $db = ComicDB_DB::db();

    if (!$result = $db->query($query)) {
      die('There was an error running the query [' . $db->error . ']');
    }
    
    $row = $result->fetch_assoc();
    
    return $row;
  }

  public function getMetaValue($publisher = null){

    $query  = "SELECT name FROM `meta_values` ";
    $query .= "WHERE id = ". $publisher ." ";

    $db = ComicDB_DB::db();

    if (!$result = $db->query($query)) {
      die('There was an error running the query [' . $db->error . ']');
    }

    $row = $result->fetch_assoc();
    
    return $row['name'];
  }

  public function getList($category = null){

    $results = [];

    $query  = "SELECT V.name, V.value, V.id as ValKey, C.id as CatKey  FROM `meta_values` as V "
      ."LEFT OUTER JOIN `meta_categories` as C "
      ."ON V.category = C.id ";
      
      if(!empty($category)){
        $query  .= "WHERE V.category = ". $category;
      }

    if($this->debug){
      Common_Debug::dumpQuery($query);
    }
    
    $db = ComicDB_DB::db();

    if (!$result = $db->query($query)) {
      die('There was an error running the query [' . $db->error . ']');
    }
    
    while ($row = $result->fetch_assoc()) {
      array_push($results, $row);
    }

    return $results;
  }

  public function updateListValue($edit = null, $id = null){
    $query = "UPDATE `meta_values` SET `name` = '$edit' WHERE id=". $id;
      if($this->debug){
        Common_Debug::dumpQuery($query);
      }
      
      $db = ComicDB_DB::db();
      $result = $db->query($query);
  }

  public function addListValue($new = null,$id = null){
    $query = "INSERT INTO meta_values (`name`,`category`) VALUES ('$new','$id')";
      if($this->debug){
        Common_Debug::dumpQuery($query);
      }
      
      $db = ComicDB_DB::db();
      $result = $db->query($query);
  }

  public function deleteListValue($id = null){
    
      $query = "DELETE FROM meta_values WHERE id=". $id;
      if($this->debug){
        Common_Debug::dumpQuery($query);
      }

      $db = ComicDB_DB::db();
    
      if (!$result = $db->query($query)) {
        die('There was an error running the query [' . $db->error . ']');
      } 

  }
  
}