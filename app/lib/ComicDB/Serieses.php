<?php

	/**
	 * This is sort of an interface class
	 */
  
  class ComicDB_Serieses extends ComicDB_Metadata {

	  private $title_id; // xid 
	  private $allSeries; // sid (all)
	  private $db;

	  public function __construct($title_id = null){
		  $this->title_id = $title_id;
	  }

		public function updateTotal(){}

		/**
		 * Returns all series for a single title (xid)
		 */
	  public function getAll(){

		  if (isset($this->allSeries)) {
			  return $this->allSeries;
		  }

		  $query = "SELECT id, title, name, publisher, category, default_price, ";
		  $query .= "first_issue, final_issue, subscribed, comments, volume, complete ";
		  $query .= "FROM series";
		  if ($this->title_id != null) {
			  $query .= " WHERE title=" . $this->title_id;
		  }
		  $query .= " ORDER BY name ASC";

		  $db = ComicDB_DB::db();
			
		  if (!$result = $db->query($query)) {
			  exit('There was an error running the query [' . $db->errno . ']');
		  }

		  $this->allSeries = [];
		  while ($row = $result->fetch_assoc()) {

			  $s = new ComicDB_Series();
			  $s->id = $row['id'];
			  $s->titleId = $row['title'];
			  $s->name = $row['name'];
			  $s->publisher = $this->getMetaValue($row['publisher']);
			  $s->category = $this->getMetaValue($row['category']);
			  $s->defaultPrice = $row['default_price'];
			  $s->firstIssue = $row['first_issue'];
			  $s->finalIssue = $row['final_issue'];
			  $s->subscribed = $row['subscribed'];
			  $s->comments = $row['comments'];
			  $s->volume = $row['volume'];
			  $s->complete = $row['complete'];

			  array_push($this->allSeries, $s);
		  }


		  return $this->allSeries;
	  }

		/**
		 * Returns single cached series (sid)
		 */
		public function getOne($id = null){
			foreach($this->allSeries as $s){
				if($s->id == $id){
					return $s;
				}
			}				
		}

		/** @todo - Figure out what this was for */
	  public function removeAllSeries(){
		  $listOfAllSeries = $this->getSeries();

		  foreach ($listOfAllSeries as $series) {
		  	$seriesIssues = new ComicDB_Issues($series->id());
			  $seriesIssues->removeAllIssues();
		  }

		  $query = "DELETE FROM series WHERE title=". $this->title_id;

		  $db = ComicDB_DB::db();
		  return $db->query($query);
	  }
  }