<?php
  /**
   * Abstract Class ComicDB_Object
   */
  
  abstract class ComicDB_Object
  {
    
    public $id;
    public $isNew; // 1 new record, 0 already saved record
    public $isDirty; // 1 to be saved, 0 has been saved
    public $isDeleted; // 1 to be deleted, 0 do not delete
    
    public function __construct($id = null){
      if (isset($id)) {
        // We want to update something but it's not
        // saved yet
        $this->id = $id;
        $this->isNew = 0;
        $this->isDirty = 1;
      } else {
        // What is is new and not saved
        $this->isNew = 1;
        $this->isDirty = 1;
      }
      // Don't delete
      $this->isDeleted = 0;
    }
    
    // Set the id when updating something
    public function id($id = null){
      if (isset($id)) {
        $this->id = $id;
        $this->isDirty = 1;
      }
      return $this->id;
    }
    
    // Calls select() sets record to dirty
    public function restore(){
      
      $rv = $this->select();
      
      $this->isNew = 0;
      $this->isDirty = 1;
      $this->isDeleted = 0;
      
      return $rv;
    }
    
    // Call $this->save() and sets isDeleted to 1
    public function remove(){
      
      $this->isDeleted = 1;
      
      return $this->save();
    }
    
    // Will call delete, insert or update
    // depending on isNew, isDirty, isDeleted attributes
    public function save(){

      if (!($this->isDirty || $this->isDeleted)) {
        return true;
      }

      if ($this->isDeleted && !$this->isNew) {
        
        $rv = $this->delete();

        $this->isNew = 0;
        $this->isDirty = 0;
        $this->isDeleted = 1;
        
      } else if ($this->isNew) {

        $rv = $this->insert();
        
        $this->isNew = 1;
        $this->isDirty = 0;
        $this->isDeleted = 0;
        
      } else {

        $rv = $this->update();
        
        $this->isNew = 0;
        $this->isDirty = 1;
        $this->isDeleted = 0;
        
      }
      
      return $rv;
    }
    
  }