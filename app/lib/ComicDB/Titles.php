<?php
  
  /**
   * Class ComicDB_Titles
   * @description - A class for all Titles
   */
  class ComicDB_Titles
  {
    
    private $titles;
    private $db;

    public $currentTitle;
    
    public function __construct(){}

	  public function filterTitles($var){

		  return ($var->id === $this->currentTitle);

	  }
    
    public function getTitles($currentTitle = null){

    	if(!empty($currentTitle)){
		    $this->currentTitle = $currentTitle;
		    return array_filter($this->titles, array($this,"filterTitles"));
	    }
      
      if (isset($this->titles)) {
        return $this->titles;
      }
      
      $query = "SELECT id, name FROM titles ORDER BY name ASC";
      
      if(empty($this->db)){
        $this->db = ComicDB_DB::db();
      }
      
      if (!$result = $this->db->query($query)) {
        echo 'There was an error running the query [' . $this->db->error . ']';
      }
      
      $this->titles = [];
      
      while ($row = $result->fetch_assoc()) {
        $t = new ComicDB_Title();
        $t->id($row['id']);
        $t->name = $row['name'];
        
        array_push($this->titles, $t);
      }
      
      $result->close();
      
      return $this->titles;
    }

  }
