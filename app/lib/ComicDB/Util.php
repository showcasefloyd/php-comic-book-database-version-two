<?php
  
  /**
   * Class ComicDB_Util
   */
  class ComicDB_Util
  {
    
    /**
     * Parse Data Function
     *
     * @param null $date
     * @return false|int|null
     */
    public static function parseDate($date = null){
      if (!isset($date) || $date == "") {
        return null;
      }

      // Oct 2001
      if (preg_match("/^[a-zA-Z0-9]+ +[a-zA-Z0-9]+$/", $date)) {
        $date = "1 $date";
      }

      // Convert a date object into a Unix timestamp
      return strtotime($date);
    }
  
    /**
     * Takes the contents of the list return from the DB and makes an
     * simple HTML dropdown list
     *
     * @param $list
     * @param $selected
     * @param int $number
     * @return string
     */
    public static function makeDropDown($list, $selected, $number = 0){
      
      $vol = "";
      $dd  = "";
      $dd .= '<option value=" "> --choose--</option>';
      
      foreach ($list as $item) {
        
        if(isset($item->volume)) {
          $vol = ' (' . $item->volume . ')';
        }
        
        if ($number == 0) {
          $name = htmlspecialchars($item->name);
          
        } else {
          $name = htmlspecialchars($item->number);
        }
        
        $id = $item->id();
        
        if ($selected == $id) {
          $dd .= '<option value="' . $id . '" selected>' . $name . $vol .'</option>';
        } else {
          $dd .= '<option value="' . $id . '">' . $name . $vol . '</option>';
        }
      }
      
      return $dd;
    }
  }