<?php
  
  class ComicDB_Issue extends ComicDB_Object
  {
    
    // The class should know what it's table columns are, we can then use this in the class
    static protected $db_columns = ['id', 'series', 'number', 'sort', 'printrun', 'quantity', 'cover_date', 'location', 'type', 'status', 'bkcondition', 'cover_price', 'purchase_price', 'purchase_date', 'guide_value', 'guide', 'issue_value', 'comments', '`thumbnail-sm`'];

    private $db;

    public $id;
    public $series;
    public $number;
    public $sort;
    public $printrun;
    public $quantity;
    public $coverDate;
    public $location;
    public $type;
    public $status;
    public $bkcondition;
    public $coverPrice;
    public $purchasePrice;
    public $purchaseDate;
    public $guideValue;
    public $guide;
    public $issueValue;
    public $comments;
    
    public function __construct($id = null){
      parent::__construct($id);
      $this->db = ComicDB_DB::db();
    }
    
    // Properties which have database columns excluding id
    
    public function attributes(){
      $attributes = [];
      foreach (self::$db_columns as $column) {
        if ($column == 'id') {
          continue;
        }
        $attributes[$column] = $this->$column;
      }
      return $attributes;
    }
  
    // Update all the properties of this Object
    public function merge_attributes($args = []){
      foreach ($args as $key => $value) {
        if (property_exists($this, $key) && !is_null($value)) {
          $this->$key = $value;
        }
      }
    }

	  /**
	   * Select single issue from collection
	   */
    protected function select(){
      $query = "SELECT * FROM issues WHERE id=" . $this->id;
    
      if (!$result = $this->db->query($query)) {
        die('There was an error running the query [' . $this->db->error . ']' . $query);
      }

	    $row = $result->fetch_assoc();

	    $this->series = $row['series'];
	    $this->number = $row['number'];
	    $this->sort = $row['sort'];
	    $this->printrun = $row['printrun'];
	    $this->quantity = $row['quantity'];
	    $this->coverDate = $row['cover_date'];
	    $this->location = $row['location'];
	    $this->type = $row['type'];
	    $this->status = $row['status'];
	    $this->bkcondition = $row['bkcondition'] ;
	    $this->coverPrice = $row['cover_price'];
	    $this->purchasePrice = $row['purchase_price'];
	    $this->purchaseDate = $row['purchase_date'];
	    $this->guideValue = $row['guide_value'];
	    $this->guide = $row['guide'];
	    $this->issueValue = $row['issue_value'];
	    $this->comments = $row['comments'];

    }
  
    protected function insert(){
    
      $query = "INSERT INTO issues (";
	    $query .= "series, number, printrun, quantity, ";
	    $query .= "cover_date, location, type, status, bkcondition, ";
	    $query .= "cover_price, purchase_price, purchase_date, guide_value, ";
	    $query .= "guide, issue_value, comments, `thumbnail-sm` ";
      $query .= ") VALUES ( ";
	    $query .= " '$this->series', '$this->number', '$this->printrun', '$this->quantity', ";
	    $query .= " '$this->coverDate', '$this->location','$this->type', '$this->status', '$this->bkcondition', ";
	    $query .= " '$this->coverPrice', '$this->purchasePrice', '$this->purchaseDate', '$this->guideValue', ";
	    $query .= " '$this->guide', '$this->issueValue', '$this->comments', '' ";
      $query .= " )";

	    $this->db->query($query);
      $this->id = $this->db->insert_id;

      if($this->debug){
        Common_debug::dumpQuery($query);
      }
      
      return $this->id;
    }
    
    protected function update(){
      $data = [];

      $data['series'] = $this->series ?? 'NULL';
      $data['number'] = $this->number ?? 'NULL';
      $data['printrun'] = $this->printrun ?? 'NULL';
      $data['quantity'] = $this->quantity ?? 'NULL';
      $data['cover_date'] = $this->coverDate ?? 'NULL';
	    $data['location'] = $this->location ?? 'NULL';
      $data['type'] = $this->type ?? 'NULL';
      $data['status'] = $this->status ?? 'NULL';
      $data['bkcondition'] = $this->bkcondition ?? 'NULL';
      $data['cover_price'] = $this->coverPrice ?? 'NULL';
      $data['purchase_price'] = $this->purchasePrice ?? 'NULL';
      $data['purchase_date'] = $this->purchaseDate ?? 'NULL';
      $data['guide_value'] = $this->guideValue ?? 'NULL';
      $data['guide'] = $this->guide ?? 'NULL';
      $data['issue_value'] = $this->issueValue ?? 'NULL';
      $data['comments'] = $this->comments ?? 'NULL';

      // build query
      $terms = [];

      foreach ($data as $key => $val) {
        array_push($terms, "$key='$val'");
      }
      $set = implode(', ', $terms);
    
      $query = "UPDATE issues SET $set WHERE id=" . $this->id;

      return $this->db->query($query);
    }
  
    protected function delete(){
      $query = "DELETE FROM issues WHERE id=" . $this->id . " LIMIT 1";

      return $this->db->query($query);
    }
  }