<?php
  
  class ComicDB_Issues
  {

    public $issues;
    public $seriesId;
    
    function __construct($seriesId)
    {
      $this->seriesId = $seriesId;
    }

    public function getLowestIssueFromSeries(){

	    $query  = "SELECT max(case when number < 0 then number end) as lowest ";
	    $query .= " FROM issues WHERE series='". $this->seriesId ."'";

	    $db = ComicDB_DB::db();
	    if (!$result = $db->query($query)) {
		    die('There was an error running the query, yo [' . $db->error . ']' . $query);
	    }

	    $row = $result->fetch_assoc();

	    $result->free_result();

	    return $row['lowest'];

    }

    public function getHighestIssueFromSeries(){

	    $query  = "SELECT max(case when number >= 0 then number end) as highest ";
	    $query .= " FROM issues WHERE series='". $this->seriesId ."'";

	    $db = ComicDB_DB::db();
	    if (!$result = $db->query($query)) {
		    die('There was an error running the query, yo [' . $db->error . ']' . $query);
	    }

	    $row = $result->fetch_assoc();

	    $result->free_result();

	    return $row['highest'];
    }

    public function getIssues(){

      if (isset($issues)) {
        return $issues;
      }
      
      $query  = "SELECT id, series, number, sort, printrun, quantity, ";
			$query .= " UNIX_TIMESTAMP(cover_date), location, type, status, bkcondition, ";
		 	$query .= " cover_price, purchase_price, UNIX_TIMESTAMP(purchase_date), ";
	 		$query .= " guide_value, guide, issue_value, comments ";
			$query .= " FROM issues WHERE series='". $this->seriesId ."'";
			$query .= " ORDER BY number ASC ";

      $db = ComicDB_DB::db();
      if (!$result = $db->query($query)) {
        die('There was an error running the query, yo [' . $db->error . ']' . $query);
      }
      
      $issues = [];

      while ($row = $result->fetch_assoc()) {
        
        $i = new ComicDB_Issue();

        $i->id = $row['id'];
        $i->series = $row['series'];
        $i->number = $row['number'];
        $i->sort = $row['sort'];
        $i->printrun = $row['printrun'];
        $i->quantity = $row['quantity'];
        $i->coverDate = $row['cover_date'];
        $i->location = $row['location'];
        $i->type = $row['type'];
        $i->status = $row['status'];
        $i->bkcondition = $row['bkcondition'];
        $i->coverPrice = $row['cover_price'];
        $i->purchasePrice = $row['purchase_price'];
        $i->purchaseDate = $row['purchase_date'];
        $i->guideValue = $row['guide_value'];
        $i->guide = $row['guide'];
        $i->issueValue = $row['issue_value'];
        $i->comments = $row['comments'];

        array_push($issues, $i);
      }

	    $result->free_result();

      usort($issues, "_inumcmp");
      
      return $issues;
    }
    
    public function removeAllIssues(){
      $query = "DELETE FROM issues WHERE series=".$this->seriesId;
      $db = ComicDB_DB::db();
      return $db->query($query);
    }

  }
  
  function _inumcmp($a, $b){
    return strnatcasecmp($a->number, $b->number);
  }