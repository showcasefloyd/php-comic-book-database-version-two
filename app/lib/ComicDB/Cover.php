<?php

class ComicDB_Cover extends ComicDB_Object {

  /**
   * Each Issue as at least one cover create for it
   */

  static protected $db_columns = ['id', 'issueId', 'description', 'artist'];

  public $id;
  public $issueId;
  public $debug = false;
  public $description;
  public $artist;

  // Pass and id for update or remove
  function __construct($id = null){
    parent::__construct($id);

    if($id){
      $this->id = $id;
    }
  }

  function select(){

    $covers = [];

    if(!$this->id){
      return;
    }

    $query = "SELECT * FROM covers where `id` = ". $this->id;
    $db = ComicDB_DB::db();

    if($this->debug){
      Common_debug::dumpQuery($query);
    }
      
    if (!$result = $db->query($query)) {
      exit('There was an error running the query [' . $db->error . ']');
    } 
    
    while($row = $result->fetch_assoc()){
      array_push($covers, $row);
    };
    
    return $covers;
  }

  function insert(){

    $query  = "INSERT INTO covers (`issueId`,`description`,`artist`) ";
    $query .= "values ($this->issueId, '$this->description', '$this->artist')";

    $db = ComicDB_DB::db();

    if($this->debug){
      Common_debug::dumpQuery($query);
    }
      
    if (!$result = $db->query($query)) {
      exit('There was an error running the query [' . $db->error . ']');
    }

    return true;
  }

  function update(){

    $query = "UPDATE covers SET description=?,artist=? WHERE id=?";
    $db = ComicDB_DB::db();
    
    try {
      $stmt = $db->prepare($query);
      if($this->debug){
        Common_debug::dumpQuery($query);
      }
      $stmt->bind_param("ssi", $this->description, $this->artist, $this->id());
      
      $stmt->execute();
      $stmt->close();
      ComicDB_DB::close_db();
      
      return true;
      
    } catch (Exception $e) {
      
      ComicDB_DB::log_exception($e);
      
      return array(
        'error' => true,
        'errmsg' => $e->getMessage()
      );
      
    }
  }

  function delete(){

    $query = "DELETE FROM covers WHERE id=? LIMIT 1";
    $db = ComicDB_DB::db();
    
    try {
      
      $stmt = $db->prepare($query);
      $stmt->bind_param("i", $this->id());
      
      $stmt->execute();
      $stmt->close();
      ComicDB_DB::close_db();
      
      return true;
      
    } catch (Exception $e) {
      ComicDB_DB::log_exception($e);
      
      return array(
        'error' => true,
        'errmsg' => $e->getMessage()
      );
      
    }
  }

}