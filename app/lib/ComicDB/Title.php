<?php
  
  class ComicDB_Title extends ComicDB_Object
  {
    
    // The class should know what it's table columns are, we can then use this in the class
    
    static protected $db_columns = ['id', 'name'];
    
    public $name;
    
    // Class for one title at a time
    public function __construct($id = null){
      parent::__construct($id);
    }
    
    // Accessors
    protected function insert(){
      
      $name = $this->name;
      $query = "INSERT INTO titles (name) VALUES ('" . $name . "')";
      
      $db = ComicDB_DB::db();
      
      if (!$result = $db->query($query)) {
        exit('There was an error running the query [' . $db->error . ']');
      }
      
      // is there a better portable way of retrieving the id?
      $query = "SELECT id FROM titles ORDER BY id DESC LIMIT 1";
      
      $db->query($query);
      
      if (!$result = $db->query($query)) {
        die('There was an error running the query [' . $db->error . ']');
      }
      
      $row = $result->fetch_assoc();
      $this->id($row['id']);
      
      return true;
    }
    
    protected function select(){
      
      $query = "SELECT id, name FROM scfloyd_comicdb.titles WHERE id=?";
      $db = ComicDB_DB::db();
      $row = [];
      
      try {
        $stmt = $db->prepare($query);
        $stmt->bind_param('i', $this->id);
        
        /* execute query */
        $stmt->execute();
        
        /* Store the result (to get properties) */
        $stmt->store_result();
        
        /* Bind the result to variables */
        $stmt->bind_result($id, $name);
        
        while ($stmt->fetch()) {
          $row['id'] = $this->id($id); // Object_DB
          $row['name'] = $this->name = $name; // Class
        }
        
        $stmt->free_result();
        
        ComicDB_DB::close_db();
        
        return true;
        
      } catch (Exception $e) {
        
        ComicDB_DB::log_exception($e);
        
        return array(
          'error' => true,
          'errmsg' => $e->getMessage()
        );
        
      }
      
    }
    
    protected function update(){
      
      $query = "UPDATE titles SET name=? WHERE id=?";
      $db = ComicDB_DB::db();
      
      try {
        $stmt = $db->prepare($query);
        $stmt->bind_param("si", $this->name, $this->id());
        
        $stmt->execute();
        $stmt->close();
        ComicDB_DB::close_db();
        
        return true;
        
      } catch (Exception $e) {
        
        ComicDB_DB::log_exception($e);
        
        return array(
          'error' => true,
          'errmsg' => $e->getMessage()
        );
        
      }
    }
    
    protected function delete(){

      /**
       * @todo - This looks dangerous, we should think of a better way
       * to handle this
       */
      $series = new ComicDB_Serieses($this->id());
      $series->removeAllSeries();

      $query = "DELETE FROM titles WHERE id=? LIMIT 1";
      $db = ComicDB_DB::db();
      
      try {
        
        $stmt = $db->prepare($query);
        $stmt->bind_param("i", $this->id());
        
        $stmt->execute();
        $stmt->close();
        ComicDB_DB::close_db();
        
        return true;
        
      } catch (Exception $e) {
        ComicDB_DB::log_exception($e);
        
        return array(
          'error' => true,
          'errmsg' => $e->getMessage()
        );
        
      }
    }
    
  }