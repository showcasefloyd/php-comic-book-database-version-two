<?php

class ComicDB_Covers extends ComicDB_Metadata {

  /**
   * Each Issue as at least one cover create for it
   */

  public $issueId;
  public $total;
  public $debug = false;

  function __construct($id = null){
    $this->issueId = $id;
  }

  function updateTotal(){}
  
  function getAll(){
    $covers = [];

    if(!$this->issueId){
      return;
    }
    $query = "SELECT * FROM covers where `issueId` = ". $this->issueId;
    $db = ComicDB_DB::db();

    if($this->debug){
      Common_debug::dumpQuery($query);
    }
      
    if (!$result = $db->query($query)) {
      exit('There was an error running the query [' . $db->error . ']');
    } 
    
    while($row = $result->fetch_assoc()){
      array_push($covers, $row);
    }
    
    return $covers;
  }
  
  function getOne(){}

}