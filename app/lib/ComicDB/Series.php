<?php
  
  class ComicDB_Series extends ComicDB_Object
  {
    
    // The class should know what it's table columns are, we can then use this in the class
    
    static protected $db_columns = ['id', 'title', 'name', 'publisher', 'category', 'default_price', 'final_issue', 'subscribed', 'complete', 'comments', 'volume'];
    
    public $titleId;
    public $name;
    public $publisher;
    public $category;
    public $defaultPrice;
    public $firstIssue;
    public $finalIssue;
    public $subscribed;
	  public $complete;
    public $comments;
    public $title;
    public $issues;
    public $volume;
    
    public function __construct($id = null){
      parent::__construct($id);
    }

    protected function select(){

		  $query = "SELECT id, title, `name`, publisher, `category`, default_price, ";
		  $query .= "first_issue, final_issue, subscribed, comments, volume, complete ";
		  $query .= "FROM series WHERE id = ?";
		  $db = ComicDB_DB::db();

		  try {
			  $stmt = $db->prepare($query);
			  $stmt->bind_param('i', $this->id);

			  /* execute query */
			  $stmt->execute();

			  /* Store the result (to get properties) */
			  $stmt->store_result();

			  /* Bind the result to variables */
			  $stmt->bind_result($id, $title, $name, $publisher, $category, $default_price, $first_issue, $final_issue, $subscribed, $comments, $volume, $complete);

			  while ($stmt->fetch()) {

				  $this->id = $id;
				  $this->titleId = $title;
				  $this->name = $name;
				  $this->publisher = $publisher;
				  $this->category = $category;
				  $this->defaultPrice = $default_price;
				  $this->firstIssue = $first_issue;
				  $this->finalIssue = $final_issue;
				  $this->subscribed = $subscribed;
				  $this->comments = $comments;
				  $this->volume = $volume;
				  $this->complete = $complete;
			  }

			  $stmt->free_result();

			  ComicDB_DB::close_db();

			  return true;

		  } catch (Exception $e) {
			  ComicDB_DB::log_exception($e);

			  return array(
				  'error' => true,
				  'errmsg' => $e->getMessage()
			  );
		  }
	  }

	  protected function insert(){
      $data = [];
      
      // mandatory fields
      $data['title'] = $this->titleId ?? 'NULL';
      $data['name'] = $this->name ?? 'NULL';
      $data['publisher'] = $this->publisher ?? 'NULL';
      $data['category'] = $this->category ?? 'NULL';
      $data['default_price'] = $this->defaultPrice ?? 'NULL';
      $data['final_issue'] = $this->finalIssue ?? 'NULL';
      $data['subscribed'] = $this->subscribed ?? 'NULL';
	    $data['complete'] = $this->complete ?? 'NULL';
      $data['volume'] = $this->volume ?? 'NULL';
      $data['comments'] = $this->comments ?? 'NULL';
      
      // build query
      $query = "INSERT INTO series ";
      $keys = array_keys($data);
      $cols = implode(', ', $keys);
      $vals = str_repeat('?,', count($keys));
      $query .= " ($cols) VALUES (" . substr($vals, 0, -1) . ")";

      $db = ComicDB_DB::db();

      try {
        $values = array();
        $stmt = $db->prepare($query);
        
        foreach ($keys as $key) {
          $values[] = $data[$key];
        }
        
        $stmt = ComicDB_DB::DynamicBindVariables($stmt, $values);
        $stmt->execute();
        $stmt->close();
        
        $this->id($db->insert_id);
        
        ComicDB_DB::close_db();
        
        return $this->id;
        
      } catch (Exception $e) {
        
        ComicDB_DB::log_exception($e);
        
        return array(
          'error' => true,
          'errmsg' => $e->getMessage()
        );
      }
    }
    
    protected function update(){
      $data = [];

	    $data['title'] = $this->titleId ?? 'NULL';
	    $data['name'] = $this->name ?? 'NULL';
	    $data['publisher'] = $this->publisher ?? 'NULL';
	    $data['category'] = $this->category ?? 'NULL';
	    $data['default_price'] = $this->defaultPrice ?? 'NULL';
	    $data['final_issue'] = $this->finalIssue ?? 'NULL';
	    $data['subscribed'] = $this->subscribed ?? 'NULL';
	    $data['complete'] = $this->complete ?? 'NULL';
	    $data['volume'] =  $this->volume ?? 'NULL';
	    $data['comments'] = $this->comments ?? 'NULL';

	    // build query
      $terms = [];
      $values = [];

      foreach ($data as $key => $value) {
        array_push($terms, "$key=?");
        $values[] = $data[$key];
      }
      $set = implode(', ', $terms);
      
      $query = "UPDATE series SET $set WHERE id=?";

	    $db = ComicDB_DB::db();
      
      try {
        
        $stmt = $db->prepare($query);
        $values[] = $this->id();
        $stmt = ComicDB_DB::DynamicBindVariables($stmt, $values);

	      $stmt->execute();
        $stmt->close();
        
        ComicDB_DB::close_db();
        
        return true;
        
      } catch (Exception $e) {
        ComicDB_DB::log_exception($e);
        return array(
          'error' => true,
          'errmsg' => $e->getMessage()
        );
      }
      
    }
    
    protected function delete() {

      $issues = new ComicDB_Issues($this->id());
      $issues->removeAllIssues();

      $query = "DELETE FROM series WHERE id=" . $this->id() . " LIMIT 1";

      $db = ComicDB_DB::db();
      return $db->query($query);
    }
  }