<?php
  /**
   * Database connection class
	 * @todo Convert all sql calls to mysqli
   * */
  
  class ComicDB_DB
  {

    public static $db;

    /**
     * Open and return a db connection
     * @return mysqli connection
     */
    public static function db(){
      
      self::$db = new mysqli(DB_URL, DB_USER, DB_PASS, DB_NAME, 3306);
      
      if (self::$db->connect_errno) {
        exit("Failed to connect to MySQL: (" . self::$db->connect_errno . ") " . self::$db->connect_error);
      }
      
      mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
      
      return self::$db;
    }
    
    /**
     * Close DB Connection
     */
    public static function close_db(){
      self::$db->close();
    }
    
    /**
     * Writes out exceptions to the php_log file
     * @param $e
     */
    public static function log_exception($e){

      if(DEV_ENV){
        Common_debug::dumpError($e);
      } else {
        error_log($e->getMessage());
        error_log("In file " . $e->getFile() . " at line " . $e->getLine());
        error_log($e->getTraceAsString());
      }
    }

		/**
		 * @param $stmt - SQL Statement Object
		 * @param $params - array of the Parameters
		 *
		 * @return mixed
		 */
    public static function DynamicBindVariables($stmt, $params){
      
      if ($params != null) {
        // Generate the Type String (eg: 'issisd')
        $types = '';
        foreach ($params as $param) {
          if (is_int($param)) {
            // Integer
            $types .= 'i';

          } elseif (is_float($param)) {
            // Double
            $types .= 'd';
          } elseif (is_string($param)) {
            // String
            $types .= 's';
          } else {
            // Blob and Unknown
            $types .= 'b';
          }
        }
        
        // Add the Type String as the first Parameter
        $bind_names[] = $types;
        
        // Loop thru the given Parameters
        for ($i = 0; $i < count($params); $i++) {
          // Create a variable Name
          $bind_name = 'bind' . $i;
          // Add the Parameter to the variable Variable
          $$bind_name = $params[$i];
          // Associate the Variable as an Element in the Array
          $bind_names[] = &$$bind_name;
        }
        
        // Call the Function bind_param with dynamic Parameters
        call_user_func_array(array($stmt, 'bind_param'), $bind_names);
      }

    

      return $stmt;


    }
  }
