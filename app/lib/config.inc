<?php

# Development
if(($_SERVER['HTTP_HOST'] == "127.0.0.1:8080" || $_SERVER['HTTP_HOST'] == "localhost:8080")){

  // Report all errors except E_NOTICE
	error_reporting(E_ALL & ~E_NOTICE);

  ini_set('display_errors', '1');  // 1 for on, 0 for off
  ini_set('log_errors','1');

  # Pointing at local Docker container
  define('DB_URL','mysql');
  define('DB_USER','scfloyd_comicdb');
  define('DB_PASS','magic');
  define('DB_NAME','scfloyd_comicdb');
  define('DEV_ENV', true);

} else {

  error_reporting(E_ERROR);

  ini_set('display_errors', '0');
  ini_set('log_errors','1');

  define('DB_URL','mysql');
  define('DB_USER','scfloyd_comicdb');
  define('DB_PASS','magic');
  define('DB_NAME','scfloyd_comicdb');
  define('DEV_ENV', false);
}

// prefixed to the title of every HTML page
define('HTML_TITLE_PREFIX', 'Artichoke, Comic Book Catalog');

// separates the title prefix and other components of the page title
define('HTML_TITLE_SEPARATOR', ' | ');

