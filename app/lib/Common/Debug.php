<?php
class Common_Debug
{

  public static function dumpQuery($query){

    echo '<div class="border border-info debug">';
    print $query .'<br>';
    echo '</div>';
  }

  public static function dumpError($query){

    echo '<div class="border border-info debug error">';
    print $query .'<br>';
    echo '</div>';
  }

  public static function dumpVars($vars){
    echo '<div class="border border-info debug error">';
    foreach($vars as $k => $v){
      print "Key: ". $k ." Var: ". $v ."<br>";
    }
    echo '</div>';
  } 
  
  public static function dumpClass($vars){
    echo '<div class="border border-info debug error">';
    var_dump($vars);
    echo '</div>';
  }  


}   

