<?php

class Catalog_Pagination {

  private $totalRecords;
  private $totalPerPage;

  public function __construct($total = null, $limit = null, $currentPage = null){
    $this->totalRecords = $total; 
    $this->totalPerPage = $limit;
    $this->printNav();
  }

  public function printNav(){

    $link = 0;
    $nav = ceil($this->totalRecords / $this->totalPerPage);
    if($nav > 1){  
      echo '<nav aria-label="Page navigation">';
      echo '<ul class="pagination">';
      for($link; $link < $nav; $link++){
        echo '<li class="page-item"><a class="page-link" href="'. $_SERVER['PHP_SELF'] .'?page='.$link.'">'.($link+1).'</a></li>';
      } 
      echo '</ul>';
      echo '</nav>';
    }

  }
}