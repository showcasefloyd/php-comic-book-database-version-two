<?php

class Catalog_Grid
{

	private $series;
	private $firstIssue;
	private $lastIssue;
	private $numberOwned;

	/**
	 * Catalog_Grid constructor.
	 * @param null $series
	 */
	function __construct($series = null)
	{

		$this->series = $series;
		$this->firstIssue = $this->series->firstIssue;
		$this->lastIssue = $this->series->finalIssue;
	}

	/**
	 * Returns an object with grid in it
	 * @return array
	 */
	public function displayGrid()
	{
		$issues = new ComicDB_Issues($this->series->id);
		$ownIssues = $issues->getIssues();

		if(!empty($this->lastIssue)){
			$last = $this->lastIssue;
		} else {
			$last = $issues->getHighestIssueFromSeries();
		}

		$this->numberOwned = count($ownIssues);

		return $this->calculateSeriesRun($ownIssues, $last);
	}

	private function countNegatives(array $array) {
    $i = 0;
    foreach ($array as $x){
      if ($x->number < 0) $i++;
    }
    return $i;
	}

	private function reduceSeriesNumbers(array $array) {
		$numbers = [];

		foreach ($array as $x){
			array_push($numbers, $x->number);
		}

		return $numbers;
	}


	private function calculateSeriesRun($issues, $last)
	{

		/**
		 * @todo if there is only one book in a collection and it's now a number one
		 * Still some small bugs to work out.
		 */

		// Making the Grid
		// * First Step
		// Grab all possible whole numbers
		// using either the defined number in the series or
		// the calculated last number
		$issueNumbers = [];
		for ($i = 1; $i <= $last; $i++) {
			array_push($issueNumbers, $i);
		}

		// * Second step
		// Reduce series to its numbers
		// Merge two sets together
		// Removed duplicate numbers
		// Sort by number
		$new = array_unique(array_merge($issueNumbers, $this->reduceSeriesNumbers($issues) ), SORT_NUMERIC);
		asort($new);

		$collection = [];
		foreach ($new as $k => $v){
			$book = [];
			$book['issue'] = $new[$k];
			$book['own'] = "N";
			$collection[] = $book;
		}

		// Each book we own
		foreach ($issues as $k => $v) {

			// Search array for the value
			foreach ($collection as $key => $book) {
				if ($book['issue'] == $v->number) {

					$collection[$key]['own'] = "Y";
					$collection[$key]['issue_id'] = $v->id;

					break;
				}
			}
		}

		return $collection;
	}



	public function calcPercentageToComplete(){

		$numberCollected = $this->numberOwned;
		$totalNumberOfIssues = $this->lastIssue;

		$percentCollected = ($numberCollected / $totalNumberOfIssues) * 100;

		return round($percentCollected) ."%";
	}

	/** Checks to see if the issue is in the read state or not read state **/
	public function hasReadIssue()
	{
	}

	/* Builds an object that takes the issue in my collection and compares
		 the issues matching any I have */
	private function formatGrid($issuesForSeries)
	{
		$countRows = 1;
		$rowAcross = 10;

		foreach ($issuesForSeries as $k => $i) {

			if ($countRows > $rowAcross) {
				$countRows = 1;
				echo "<hr class='clear-row'>";
			}

			if ($i['own'] == "Y") {
				echo "<div class='issue-box own' ><a href='./issue.php?iid=" . $i['issue_id'] . " '  target='issue'> " . $i['issue'] . "</a></div>";
			} else {
				echo "<div class='issue-box'>" . $i['issue'] . "</div>";
			}

			$countRows++;
		}
	}

}
