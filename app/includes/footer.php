<footer class='footer'>Rad Planet Software, LLC</footer>
<?php

if(DEV_ENV){
  print "<footer class='debug_footer' >";
  foreach($_SERVER as $key => $value){
  	print "<pre>";
    print "<b> ". $key ."</b> = ". $value ."<br>";
    print "</pre>";
  }
	print "</footer>";
}