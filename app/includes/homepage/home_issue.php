<div>
	<table class="table-striped table-bordered issue-table">
		<tr>
			<td class="issue-key">Issue Number:</td>
			<td class="issue-val"><?= $number; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Print Run:</td>
			<td class="issue-val"><?= $printRun; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Cover Date:</td>
			<td class="issue-val"><?= $coverDate; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Issue Type:</td>
			<td class="issue-val"><?= $type; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Location:</td>
			<td class="issue-val"><?= $location; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Quantity:</td>
			<td class="issue-val"><?= $quantity; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Status:</td>
			<td class="issue-val"><?= $status; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Condition:</td>
			<td class="issue-val"><?= $condition; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Cover Price:</td>
			<td class="issue-val"><?= $coverPrice; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Purchase Price:</td>
			<td class="issue-val"><?= $purchasePrice; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Price Guide Value:</td>
			<td class="issue-val"><?= $priceGuideValue; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Issue Value:</td>
			<td class="issue-val"><?= $issueValue; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Purchase Date:</td>
			<td class="issue-val"><?= $purchaseDate; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Price Guide:</td>
			<td class="issue-val"><?= $pricegGuide; ?></td>
		</tr>
		<tr>
			<td class="issue-key">Comments:</td>
			<td class="issue-val"><?= $comments; ?></td>
		</tr>
	</table>
</div>