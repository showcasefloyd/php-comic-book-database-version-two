<div class="card mb-4">
  <div class="card-header">
    <div class="lead"><?= htmlspecialchars($series->name); ?></div>
    <div><?= htmlspecialchars($series->publisher); ?></div>
  </div>
  <div class="card-body">

    <div class="card-group">
      <div class="card">
        <div class="card-body">

          <p class="card-text"><b>Volume</b> <?= $volume ?></p>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <p class="card-text"><b>Number of Issues</b> <?= $lastIssue ?></p>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <p class="card-text"><b>Percentage Complete</b> <?= $percentComplete ?></p>
        </div>
      </div>
    </div>

    <hr>

    <div class="series-series"><b>Category:</b>  <?= $category ?> </div>
    <div class="series-series"><b>Completed:</b>
      <span class="label label-primary"><?= ($complete == 0) ? ' No ' : ' Yes '; ?> </span></div>
    <div class="series-series"><b>Notes:</b> <?= $comments ?> </div>
  </div>
</div>


<div id="comicgrid" class="mb-4">
	<?php
	foreach ($gridData as $key => $i) {
		if ($i['own'] == "Y") {

			echo "<div class='issue-box own'><a href='./index.php?iid=" . $i['issue_id'] . "&sid=" . $sid . "&xid=" . $xid . "'> " . $i['issue'] . "</a></div>";

		} else {

			if (!empty($i['issue'])) {
				echo "<div class='issue-box'>" . $i['issue'] . "</div>";
			}
		}
	}
	?>
</div>