<div class="container-fluid">
  <div class="row">
    <div class="col-12">

      <div class="card my-4">
        <div class="card-body">

          <p class="lead">Analyze your collection</p>
          <p>To start analyzing your amazing collection. Just begin by selecting a Title from the left side bar
            menu.</p>
        </div>

      </div>

    </div>
  </div>

  <div class="row">
    <div class="col-6">

      <div class="card">
        <div class="card-body">
          <p class="lead">Your Collection</p>

          <div><span class="badge badge-primary"><?= $collectionStats["totalPublisher"] ?></span> Publishers</div>
          <div><span class="badge badge-primary"><?= $collectionStats["totalTitles"] ?></span> Titles</div>
          <div><span class="badge badge-primary"><?= $collectionStats["totalSeries"] ?></span> Series</div>
          <div><span class="badge badge-primary"><?= $collectionStats["totalIssues"] ?></span> Issues</div>
        </div>
      </div>
    </div>

    <div class="col-6">
      <div class="card">
        <div class="card-body">
          <p class="lead">Most Recent Update</p>

          <div>Publisher: <?= $lastUpdate["publisher"] ?></div>
          <div>Series: <?= $lastUpdate["name"] ?></div>
          <div>Issue Number: <?= $lastUpdate["number"] ?></div>
          <div>Updated On: <?= date('M-d-Y h:m:s a', strtotime($lastUpdate["last_change"])) ?></div>
        </div>
      </div>

    </div>

  </div>
</div>