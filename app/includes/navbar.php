<?php
if(DEV_ENV) {
	echo printDevBanner();
  echo printVarBanner($_GET, 'Get');
  echo printVarBanner($_POST, 'Post');
}
?>

<div class="page-header">

  <h4 class="header"><? echo $page_title; ?></h4>

  <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/index.php">Library</a></li>
    <li class="breadcrumb-item"><a href="/administer/index.php">Admin Panel</a></li>
    <li class="breadcrumb-item"><a href="/imports.php">Imports</a></li>
    <li class="breadcrumb-item"><a href="/reports.php">Reports</a></li>
  </ol>
  </nav>
</div>

