<?
require_once('./lib/global.inc');

$page_title = ComicDB_pageTitle("Imports");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><? echo $page_title; ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="css/main.css" media="screen">
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col">
			<? require_once('includes/navbar.php'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col" id="left-menu">

			<p>Coming Soon</p>
		</div>
	</div>
</div>

</body>
</html>