#!/bin/bash

echo "Updating Artichoke Comic Database";

FILE1=$1
PW='localdbpass'
DB='scfloyd_comicdb'
CO='scfloyd_comicdb'

if [ -z "$FILE1" ]
then
  echo "You must pass in one script";
fi

docker exec -i $CO mysql -uroot -p$PW --database=$DB < $FILE1