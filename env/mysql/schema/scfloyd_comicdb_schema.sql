-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Apr 13, 2021 at 11:05 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scfloyd_comicdb`
--
CREATE DATABASE IF NOT EXISTS `scfloyd_comicdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `scfloyd_comicdb`;

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

DROP TABLE IF EXISTS `issues`;
CREATE TABLE `issues` (
  `id` int(11) NOT NULL,
  `series` int(11) NOT NULL DEFAULT '0',
  `number` varchar(80) NOT NULL DEFAULT '',
  `sort` int(11) DEFAULT NULL,
  `printrun` varchar(80) DEFAULT '1st Printing',
  `quantity` int(11) DEFAULT '1',
  `cover_date` date DEFAULT NULL,
  `location` varchar(80) DEFAULT NULL,
  `type` varchar(80) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `bkcondition` varchar(15) DEFAULT NULL,
  `cover_price` decimal(9,2) DEFAULT NULL,
  `purchase_price` decimal(9,2) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `guide_value` decimal(9,2) DEFAULT NULL,
  `guide` varchar(80) DEFAULT NULL,
  `issue_value` decimal(9,2) DEFAULT NULL,
  `comments` text,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_change` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `thumbnail-sm` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meta_categories`
--

DROP TABLE IF EXISTS `meta_categories`;
CREATE TABLE `meta_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Types of metadata';

-- --------------------------------------------------------

--
-- Table structure for table `meta_values`
--

DROP TABLE IF EXISTS `meta_values`;
CREATE TABLE `meta_values` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `category` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Values that make up the Metadata';

-- --------------------------------------------------------

--
-- Table structure for table `print_run`
--

DROP TABLE IF EXISTS `print_run`;
CREATE TABLE `print_run` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
CREATE TABLE `publisher` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `last_update` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_change` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `series`
--

DROP TABLE IF EXISTS `series`;
CREATE TABLE `series` (
  `id` int(11) NOT NULL,
  `title` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `publisher` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `default_price` decimal(9,2) DEFAULT NULL,
  `first_issue` int(11) DEFAULT NULL,
  `final_issue` int(11) DEFAULT NULL,
  `subscribed` tinyint(1) DEFAULT '0',
  `comments` text,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_change` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `volume` varchar(100) DEFAULT NULL,
  `complete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stash-my-comics-import`
--

DROP TABLE IF EXISTS `stash-my-comics-import`;
CREATE TABLE `stash-my-comics-import` (
  `Title` varchar(72) DEFAULT NULL,
  `Publisher` varchar(32) DEFAULT NULL,
  `Issue #` varchar(11) DEFAULT NULL,
  `Book Title` varchar(25) DEFAULT NULL,
  `Stories` varchar(324) DEFAULT NULL,
  `Comments` varchar(114) DEFAULT NULL,
  `PublishedDate` varchar(23) DEFAULT NULL,
  `# of IssuesI Own` varchar(1) DEFAULT NULL,
  `NewsstandPrice` varchar(5) DEFAULT NULL,
  `Newsstand PriceCurrency` varchar(3) DEFAULT NULL,
  `Newsstand PriceSub-Total` decimal(7,2) DEFAULT NULL,
  `Price I Paid` varchar(5) DEFAULT NULL,
  `Price I PaidCurrency` varchar(3) DEFAULT NULL,
  `Price I PaidSub-Total` varchar(6) DEFAULT NULL,
  `CurrentValue` varchar(8) DEFAULT NULL,
  `Current PriceCurrency` varchar(3) DEFAULT NULL,
  `Current ValueSub-Total` varchar(8) DEFAULT NULL,
  `Stashed InCategory` varchar(25) DEFAULT NULL,
  `Condition` varchar(5) DEFAULT NULL,
  `Graded By` varchar(10) DEFAULT NULL,
  `Grade` varchar(10) DEFAULT NULL,
  `My Notes` varchar(19) DEFAULT NULL,
  `Image` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `stats`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `stats`;
CREATE TABLE `stats` (
`totalPublisher` bigint(21)
,`totalTitles` bigint(21)
,`totalSeries` bigint(21)
,`totalIssues` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

DROP TABLE IF EXISTS `titles`;
CREATE TABLE `titles` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_change` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `stats`
--
DROP TABLE IF EXISTS `stats`;

DROP VIEW IF EXISTS `stats`;
CREATE ALGORITHM=UNDEFINED DEFINER=`scfloyd_comicdb`@`%` SQL SECURITY DEFINER VIEW `stats`  AS SELECT (select count(distinct `p`.`name`) from `publisher` `p`) AS `totalPublisher`, (select count(distinct `t`.`name`) from `titles` `t`) AS `totalTitles`, (select count(distinct `s`.`name`) from `series` `s`) AS `totalSeries`, (select count(`i`.`number`) from `issues` `i`) AS `totalIssues` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `series` (`series`),
  ADD KEY `number` (`number`);

--
-- Indexes for table `meta_categories`
--
ALTER TABLE `meta_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `meta_values`
--
ALTER TABLE `meta_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `print_run`
--
ALTER TABLE `print_run`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`),
  ADD KEY `name` (`name`),
  ADD KEY `publisher` (`publisher`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `issues`
--
ALTER TABLE `issues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meta_categories`
--
ALTER TABLE `meta_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meta_values`
--
ALTER TABLE `meta_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `print_run`
--
ALTER TABLE `print_run`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
