
const { gulp, src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const php = require('gulp-connect-php');

sass.compiler = require('node-sass');

function cssTranspile(cb) {
  return src('sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('.'))
    .pipe(dest('./app/css'));
}

function browsersyncReload(cb){
  browserSync.reload();
  cb();
}

function browsersyncServe(cb){
  browserSync.init({
    proxy: '127.0.0.1:8080',
    port: 3000,
    open: true,
    notify: false
  });
  cb();
}

// Watch Task
function watchTask(){
  watch(['sass/**/*.scss'], series(cssTranspile, browsersyncReload));
  watch(['app/**/*.php','app/**/*.inc'], browsersyncReload);
}

exports.browsersyncServe = browsersyncServe;
exports.cssTranspile = cssTranspile;

exports.default = series(
  cssTranspile,
  browsersyncServe,
  watchTask,
);

